"""Post admin classes"""
# Django
from django.contrib import admin

# Models
from posts.models import Post

# Register your models here.
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('pk','title', 'user','photo','created','modified')
    list_display_links = ('pk',)
    list_editable = ('photo',)
    list_filter = (
        'created',
        'modified'
        )