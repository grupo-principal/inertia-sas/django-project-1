FROM python:3.8

RUN apt-get update
# Para poder utilizar remote_pdb
RUN apt-get -y install telnet


COPY ["requirements.txt","requirements.txt"]

RUN pip3 install -r requirements.txt

ENV WEBAPP_DIR=/code

WORKDIR $WEBAPP_DIR

COPY ["./code","."]

CMD ["python3","manage.py","runserver","0.0.0.0:8000"]
# CMD tail -f /dev/null 