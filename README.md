# Django New Instagram
## Contenido
- [Iniciar con Docker](#iniciar-con-docker)
- [Entorno de Trabajo](#entorno-de-trabajo)
- [Configuración Inicial](#configuración-inicial)
- [El Debugger de Python](#el-debugger-de-python)
- [Argumentos en la URL](#argumentos-en-la-url)
- [Templates](#templates)
- [Modelo Template Vista (MTV) de Django](#modelo-template-vista-mtv-de-django)
- [Modelos](#modelos)
- [El ORM de Django](#el-orm-de-django)
- [Modelo de usuario de Django](#modelo-de-usuario-de-django)
- [Usuarios Personalizados](#usuarios-personalizados)
- [Modificar Dash Board de Administración](#modificar-dash-board-de-administración)
- [Registrar Usuario Customizado en Admin de Django](#registrar-usuario-customizado-en-admin-de-django)
- [Modelo de Post](#modelo-de-post)
- [Acceder a imágenes guardadas](#acceder-a-imágenes-guardadas)
- [Templates y archivos estáticos](#templates-y-archivos-estáticos)
- [Login](#login)
- [Ajuste de estilos del login](#ajuste-de-estilos-del-login)
- [Logout](#logout)
- [Signup](#signup)
- [Middleware](#middleware)
- [Formularios](#formularios)
- [Model forms](#model-forms)
- [Validación de Formularios](#validación-de-formularios)
- [Protegiendo la vista de perfil, Detail View y Lis View](#protegiendo-la-vista-de-perfil-detail-view-y-lis-view)
- [CreateView, FormView y UpdateView](#createview-formview-y-updateview)
- [Generic auth views](#generic-auth-views)
- [Class-based views](#class-based-views)
- [Glosario](#glosario)

## Iniciar con Docker 
### Iniciar contenedor
```
docker-compose up --build -d
```
- Crea e inicia el contenedor (`up`).
- Crea la imagen antes de crear el contenedor (`--build`)
- Modo separado (detached): ejecuta el contenedor en segundo plano (`-d`, `--detach`).
#### Conectarse nuevamente con el contenedor
```
docker-compose up --attach-dependencies
```
### Ingresar al bash del contendor
Ejecuta (`exec`) el comando `bash` en el contenedor en ejecución (`web`).
```
docker-compose exec web bash
```
### Mostrar logs de salida
```
docker-compose logs -f
```
- Muestra el log de salida del contenedor (`logs`)
- Se mantiene siguiendo la salida (`-f`, `--follow`)

### Reiniciar el contenedor
```bash
docker-compose restart
```
### Documentación
- [docker-compose up](https://docs.docker.com/compose/reference/up/)
- [docker-compose exec](https://docs.docker.com/compose/reference/exec/)
- [docker-compose logs](https://docs.docker.com/compose/reference/logs/)
## Historia de Django
Nace en 2003, con la necesidad de hacer web’s con la filosofía de hacer las cosas de manera agil.

- Poder hacer sitios escalables.
- URLs bien diseñadas.
- HTTP request y responses.
- Muchos sitios en poco tiempo.
- ORM, que es conectar a na DB a traves de una interfaz python.

Características.
- Rápido desarrollo.
- Listo para todo.
- Seguro contra ataques.
- Escalable.
- Versátil.

Ventajas
- Es desarrollado en Python.
- DRY(Don’t repeat yourself).
- Comunidad Open Source.

### Lecturas recomendadas
- [What is the history of the Django web framework? Why has it been described as "developed in a newsroom"?](https://www.quora.com/What-is-the-history-of-the-Django-web-framework-Why-has-it-been-described-as-developed-in-a-newsroom/answer/Simon-Willison)





## Entorno de Trabajo
### Ubuntu 20.04
#### Instalación pip3
##### Actualizar el sistema
```
sudo apt-get update
```
##### Instalar `pip3`
```
sudo apt-get -y install python3-pip
```
#### Entorno virtual
##### Instalar paquete para entornos virtuales en Ubuntu (en MAC no es necesario)
```
sudo apt-get install python3.8-venv
```
##### Instalar el entorno virtual
```
python3 -m venv .env
```
##### Crea proyecto
```bash
mkdir django-project
cd django-project
```
##### Activar el entorno virtual
```
source ../.env/bin/activate
```
##### Desactivar el entorno virtual
```
deactivate
```
### Instalación de paquetes con requirements.txt
#### requirements.txt
```
Django
```
#### Instalación
```
pip3 install -r requirements.txt
```
#### Verificar librerías
```
pip3 freeze
```
### Dockerfile
```
FROM python:3.8
COPY ["requirements.txt","requirements.txt"]
RUN pip3 install -r requirements.txt
ENV WEBAPP_DIR=/code
WORKDIR $WEBAPP_DIR
```





## Configuración Inicial
- `pip freeze `: para validar las extensiones instaladas.
- `Django-admin startproject nombreproyecto .`: para la creación del proyecto.
### Crear nuevo proyecto
Ejecutar el comando `startproject`
```
django-admin startproject webapp
```
Para crear un proyecto con la siguiente estructura
```
.
├── manage.py
└── webapp
    ├── asgi.py
    ├── __init__.py
    ├── settings.py
    ├── urls.py
    └── wsgi.py
```
#### webapp/\_\_init\_\_.py
Declarar a `webapp` como un módulo de python.

#### webapp/settings.py
Define todas las configuraciones del proyecto.
##### Ubicación del proyecto
```py
# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
```
##### Seguridad
Utilizado en el hashing de contraseñas y las sesiones que se almacenan en la base de datos.
```py
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-0nhx=t6r&_&tg-w3$sq&etj-k5o1j$+!98trpov(tccd!6+9n)'
```
##### Proyecto en Desarrollo
```py
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
```
##### Hosts permitidos
```py
ALLOWED_HOSTS = []
```
##### Aplicaciones ligadas al proyecto de django
```py
# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```
##### Definir archivo principal de URLs
```py
ROOT_URLCONF = 'webapp.urls'
```
##### Configuración de templates
```py
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```
##### Archivo WSGI
```py
WSGI_APPLICATION = 'webapp.wsgi.application'
```
##### Configuración de la base de datos
```py
# Database

# https://docs.djangoproject.com/en/3.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
```
##### Validadores de contraseña
```py
# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
```
##### Internacionalización
```py
# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
```
##### Archivos estáticos
```py
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/
STATIC_URL = '/static/'
```
##### Tipo de campo de la llave primaria
```py
# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
```
#### webapp/urls.py
Define los puntos de entrada para todas la peticiones de django.

#### webapp/wsgi.py y webapp/asgi.py
Archivos usados durante el deployment para producción y son la interfaz wsgi y asgi con el proyecto de django cuando el servidor está corriendo en producción

#### manage.py
Es un archivo que no se ajusta, pero es el archivo sobre el cual se interactúa durante todo el desarrollo. Es una interfaz sobre django admin.

##### Listar subcomandos de `manage.py`
```
python3 manage.py
```
### Correr el servidor
```
python3 manage.py runserver
```
### Dockerfile
```docker
FROM python:3.8
COPY ["requirements.txt","requirements.txt"]
RUN pip3 install -r requirements.txt
ENV WEBAPP_DIR=/code
WORKDIR $WEBAPP_DIR
COPY ["./code","."]
CMD ["python3","manage.py","runserver","0.0.0.0:8000"]
```
### docker-compose.yml
```yml
version: '3.8'

services:
  web:
    build:
      context: .
    ports: 
      - 8000:8000
    volumes:
      - ./code:/code
```
### urls.py
```py
from django.contrib import admin
from django.urls import path
from django.http import HttpResponse

def hello_world(request):
    return HttpResponse('Hello, world!')

urlpatterns = [
    path('hello-world', hello_world),
]
```
### Lectura recomendada
- [Django Documentation](https://docs.djangoproject.com/en/2.0/ref/settings/)





## Request
Creación de vistas, patrones de urls, procesamiento de las peticiones.
### Vistas (views)
Django llama vistas a las funciones que responden a una petción http.

Se crea el archivo `views.py` para contener dichas funciones.
```
.
├── manage.py
    ├── views.py
    :   :   :
    :   :   :
```
### urls.py
```py
from django.contrib import admin
from django.urls import path
> from webapp import views

urlpatterns = [
>   path('hello-world', views.hello_world),
>   path('hi',views.hi),
]
```
### views.py
```py
"""Webapp view."""
# Django
from django.http import HttpResponse, JsonResponse
# Utilities
from datetime import datetime

def hello_world(request):
    """Return a greeting"""
    return HttpResponse('Current server time is {now}'.format(
        now=datetime.now().strftime('%b %dth, %Y - %H:%M hrs')
        ))

def hi(request):
    """Hi."""
    numbers = request.GET['numbers']
    return HttpResponse(str(numbers))
```
### Lecturas recomendadas
- [Django Documentation - Request](https://docs.djangoproject.com/en/2.0/ref/request-response/)





## El Debugger de Python
El módulo `pdb` define un depurador de código fuente interactivo para programas Python. 

Admite la configuración (condicional) de puntos de interrupción y el paso único en el nivel de la línea de origen, la inspección de marcos de pila, la lista de código fuente y la evaluación de código Python arbitrario en el contexto de cualquier marco de pila. También es compatible con la depuración post-mortem y se puede llamar bajo el control del programa.

### Ejemplo `pdb` (fuera de Docker)
#### views.py

```py
:   :   :
:   :   :
def hi(request):
    """Hi."""
    numbers = request.GET['numbers']
>   import pdb; db.set_trace()
    return HttpResponse(str(numbers))
```
### Uso de PDB con Docker
Es importante establecer los parámetros `stdin_open` y `tty` como iguales `true` en cada servicio que usará PDB.

Sin embargo, aparecerán problemas cuando establezca puntos de interrupción por medio de un encantador pdb de la biblioteca estándar de Python y, junto con él, tendrá la generación continua de registros en STDOUT. 

Aunque se puede conectar (attach) con un contenedor de la ventana acoplable y saltar a éste, no se podrá escribir nada, porque los registros siempre anularán la entrada.

La solución es utilizar la biblioteca `remote-pdb` y depurarla de forma remota mediante `telnet`.

### `pdm-remote`
PDB remoto. Para usar algún host (por ejemplo `127.0.0.1`) o puerto específico (por ejemplo `4444`) se utiliza:
```python
import remote_pdb
remote_pdb.set_trace('127.0.0.1', 4444)
```
### Ejemplo `remote_pdb` (dentro de Docker)
#### views.py

```py
:   :   :
:   :   :
def hi(request):
    """Hi."""
    numbers = request.GET['numbers']
    # import pdb; pdb.set_trace()
    import remote_pdb; remote_pdb.set_trace(host='0.0.0.0', port=4444)
    return HttpResponse(str(numbers))
```
#### docker-compose.yml
Se deben incluir `stdin_open: true` y `tty: true` (los cuales son equivalentes a `docker exec -it`). 
```yml
version: '3.8'

services:
  web:
    build:
      context: .
    ports: 
      - 8000:8000
>   stdin_open: true
>   tty: true
    volumes:
```
#### requirements.txt
Se debe incluir el paquete `remote-pdb` dentro de python.
```txt
Django
> remote-pdb
```
#### Dockerfile
Se debe instalar `telnet` para la comunición con `remote-pdb`.
#### Bash
Primero se ejecuta en bash el cual se comunicará con `remote-pdb`
```bash
docker-compose exec web bash
```
Y una vez se haya ingresado al container, correr `telnet`
```bash
telnet 0.0.0.0 4444
```
### Lecturas recomendadas
- [pdb — The Python Debugger](https://docs.python.org/3/library/pdb.html)
- [How to use PDB inside a docker container.](https://vladyslav-krylasov.medium.com/how-to-use-pdb-inside-a-docker-container-eeb230de4d11)
- [Preferred way to run docker-compose in interactive mode](https://forums.docker.com/t/preferred-way-to-run-docker-compose-in-interactive-mode/8450/8)
- [Docker run reference > Detached vs foreground > Foreground](https://docs.docker.com/engine/reference/run/#foreground)
- [Compose file version 3 reference > domainname, hostname, ipc, mac_address, privileged, read_only, shm_size, stdin_open, tty, user, working_dir](https://docs.docker.com/compose/compose-file/compose-file-v3/#domainname-hostname-ipc-mac_address-privileged-read_only-shm_size-stdin_open-tty-user-working_dir)





## Argumentos en la URL
Ordenar los números pasados desde la URL, responder a la petición en formato Json y otras formas de pasar argumentos a través de la URL.
### urls.py
```py
from django.contrib import admin
from django.urls import path
from webapp import views
urlpatterns = [
    path('hello-world', views.hello_world),
>   path('sorted',views.sort_numbers),
>   path('hi/<str:name>/<int:age>/', views.say_hi)
]
```
### views.py
```py
# Django
from django.http import HttpResponse, JsonResponse
> import json
# Utilities
from datetime import datetime

def hello_world(request):
    """Return a greeting"""
    return HttpResponse('Current server time is {now}'.format(
        now=datetime.now().strftime('%b %dth, %Y - %H:%M hrs')
        ))

> def sort_numbers(request):
>    """Return a JSON response with sorted numbers"""
>    numbers = [int(i) for i in request.GET['numbers'].split(',')]
>    sorted_ints = sorted(numbers)
>    # import remote_pdb; remote_pdb.set_trace(host='0.0.0.0', port=4444)
>    data = {
>        'status': 'ok',
>        'numbers': sorted_ints,
>        'message': 'Integers sorted successfully.'
>
>    }
>    return HttpResponse(json.dumps(data, indent=4), content_type='application/json')

> def say_hi(request, name, age):
>    '''Return a greeting'''
>    if age < 12:
>        message = 'Sorry {}, your are not allowed here'.format(name)
>    else:
>        message = 'Hello {}!, Welcome to Instagram'.format(name)
>    return HttpResponse(message)
```
### Lecturas recomendadas
- [Django Documentation - Request](https://docs.djangoproject.com/en/2.0/ref/request-response/)
- [Django Documentation - URL Dispatcher](https://docs.djangoproject.com/en/2.0/topics/http/urls/)




## Aplicación
Una app dentro de Django es un modulo de python que provee un conjunto de funcionalidades relacionadas entre sí.
Las apps son una combinación de models, vistas, urls, archivos estaticos.

Muchas apps en django estan hechas para ser reutilizadas.
### Crear nueva aplicación
```bash
python3 manage.py startapp posts
```
Para crear un proyecto con la siguiente estructura
```
.
├── manage.py
├── posts
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
└── webapp
    ├── __init__.py
    :   :   :
    :   :   :
```
#### post/\_\_init\_\_.py
Declarar a `posts` como un módulo de python.
#### post/migrations/
Es un módulo de python que se encarga de grabar los cambios en la base de datos.
##### post/migrations/\_\_init\_\_.py
Declarar a `migrations` como un módulo de python.
#### post/admin.py
Se encarga de registrar los modelos en el administrador de Django
```py
from django.contrib import admin
# Register your models here.
```
#### post/apps.py
Declara toda la configuración de la app hacia el público en caso que la app sea reutilizable (y aunque no sea reutilizable, dentro de este archivo se pueden configurar ciertas variables).
```py
from django.apps import AppConfig

class PostsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'posts'
```
#### post/models.py
Define los modelos de los datos.
```py
from django.db import models
# Create your models here.
```
#### post/views.py
Para realizar las vistas.
```py
from django.shortcuts import render
# Create your views here.
```
#### post/tests.py
Para realizar las pruebas.
```py
from django.test import TestCase
# Create your tests here.
```
### Configurar la aplicación
Se va a configurar la aplicación en `posts/apps.py`
- El nombre común de la aplicación
- El nombre en plural

Se va a instalar la apliación en `webapp/settings.py`

#### posts/apps.py
```py
> '''Posts application module'''
from django.apps import AppConfig

class PostsConfig(AppConfig):
>   '''Post application settings.'''
    default_auto_field = 'django.db.models.BigAutoField'
>   name = 'posts' # se mantiene el mismo nombre
>   varbose_name = 'Posts'
```

#### webapp/settings.py
```py
:   :   :
:   :   :
# Application definition

INSTALLED_APPS = [
>   # Django apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

>   # Local apps
>   'posts',

]
:   :   :
:   :   :
```
### Crear primera vista
La vista va a pintar un html como si fuera un script.

Se debe:
- Registrar URL en `webapp/urls.py`

#### webapp/urls.py
```py
from django.contrib import admin
from django.urls import path
> from webapp import views as local_views
> from posts import views as posts_views

urlpatterns = [
>   path('hello-world', local_views.hello_world),
>   path('sorted',local_views.sort_numbers),
>   path('hi/<str:name>/<int:age>/', local_views.say_hi),
>
>   path('posts/', posts_views.list_posts),
]
```
#### posts/views.py
```py
'''Posts views.'''
# Django
from django.http import HttpResponse

# Utilities
from datetime import datetime

posts = [
    {
        'name': 'Mont Blac',
        'user': 'Yésica Cortés',
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'picture': 'https://picsum.photos/200/200/?image=1036'
    },
    {
        'name': 'Via Láctea',
        'user': 'C. Vander',
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'picture': 'https://picsum.photos/200/200/?image=903',
    },
    {
        'name': 'Nuevo auditorio',
        'user': 'Thespianartist',
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'picture': 'https://picsum.photos/200/200/?image=1076',
    }
]

# Create your views here.
def list_posts(request):
    """List existing posts."""
    content = []
    for post in posts:
        content.append("""
            <p><strong>{name}</strong></p>
            <p><small>{user} - <i>{timestamp}</i></small></p>
            <figure><img src="{picture}"/></figure>
        """.format(**post))
    return HttpResponse('<br>'.join(content))
```
### Lecturas recomendas
- [Django Documentation - Applications](https://docs.djangoproject.com/en/2.0/ref/applications/)




## Templates
Template system de Django es una manera de presentar los datos usando HTML, está inspirado en Jinja2 y su sintaxis, por lo cual comparte muchas similitudes. Permite incluir alguna lógica de programación.

- Los templates se definen en el archivo de `settings.py`. Se mantiene la configuración existente.
- Se crea un folder `templates` dentro de 'post'.
- Se crear un archivo `feed.html` dentro de `templates`.
### posts/templates
```
.
├── manage.py
├── posts
│   ├── templates
│   │   └── feed.html
│   └── views.py
:   :   :
:   :   :
└── webapp
    ├── settings.py
:   :   :
:   :   :
```
### webapp/settings.py
```py
:   :   :
:   :   :
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
:   :   :
:   :   :
```
### posts/views.js
```py
'''Posts views.'''
# Django
> from django.shortcuts import render

# Utilities
from datetime import datetime


posts = [
    {
>       'title': 'Mont Blanc',
>       'user': {
>           'name': 'Yésica Cortés',
>           'picture': 'https://picsum.photos/60/60/?image=1027'
>       },
>       'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
>       'photo': 'https://picsum.photos/800/600?image=1036',
>   },
>   {
>       'title': 'Via Láctea',
>       'user': {
>           'name': 'Christian Van der Henst',
>           'picture': 'https://picsum.photos/60/60/?image=1005'
>       },
>       'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
>       'photo': 'https://picsum.photos/800/800/?image=903',
>   },
>   {
>       'title': 'Nuevo auditorio',
>       'user': {
>           'name': 'Uriel (thespianartist)',
>           'picture': 'https://picsum.photos/60/60/?image=883'
>       },
>       'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
>       'photo': 'https://picsum.photos/500/700/?image=1076',
    }
]

# Create your views here.
def list_posts(request):
    """List existing posts."""
>   return render(request, 'feed.html',{'posts': posts})

```
### posts/templates/feed.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Instagram</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <br><br>
    <div class="container">
        <div class="row">
            {% for post in posts %}
                <div class="col-lg-4 offset-lg-4">
                    <div class="media">
                        <img class="mr-3 rounded-circle" src="{{ post.user.picture }}" alt="{{ post.user.name }}">
                        <div class="media-body">
                            <h5 class="mt-0">{{ post.user.name }}</h5>
                            {{ post.timestamp }}
                        </div>
                    </div>
                    <img class="img-fluid mt-3 border rounded" src="{{ post.photo }}" alt="{{ post.title }}">
                    <h6 class="ml-1 mt-1">{{ post.title }}</h6>
                </div>
            {% endfor %}
        </div>
    </div>
    
</body>
</html>
```
### Lecturas recomendadas
- [Django Documentation - Built-in template tags and filters](https://docs.djangoproject.com/en/2.0/ref/templates/builtins/)




## Modelo Template Vista (MTV) de Django
Un patrón de diseño, en términos generales, es una solución reutilizable a un problema común.
El patrón más común para el desarrollo web es MVC (Model, View, Controller). Django implementa un patrón similar llamado MTV (Model, Template, View).
### MVC (Model View Controller)
- Model: define estructura, acceso y validación de datos
- View: presenta los datos
- Controller: trata el request, actualiza los datos
```
         MODEL
         /   ↖
        /     \
  Updates     Manipulates
       |       |
       ↓       |
     VIEW    CONTROLLER
       |       ↑
       |       |
     Sees    Uses
        \    /
         ↘  /
         USER
```
### MTV => MVC
- Views + Urls => Controller
  - `urls.py`: Maneja la parte de rutas.
  - `views.py`: Maneja el request de un usuario.
- Templates => View
  - `<template>.html`: Se renderiza a través del sistema de templates de django.
- Models => Model
  - `models.py`: Son los modelos dentro de nuestra aplicación.
### Model
Es la forma en la que creamos esquemas de objetos (un usuario, un post, etc) para representarlos en nuestra base de datos. 

El modelo sin importar nuestro sistema ge BD (mysql, postgress, etc) nos ayudara a crear esta entidad a través de un OMR, esto nos ahorra la molestia de tener que escribir las sentencias de SQL para crear las tablas y atributos.
### Template
 Es el encargado de manejar la lógica y sintaxis de la información que se va a presentar en el cliente, el sistema de templates de django usa HTML para ello.
### View
Su función es solo suministrar datos al template
Manda la información necesaria el template para que este pueda manejar los datos y presentarlos de una manera correcta.




## Modelos
El Modelo en Django usa diferentes opciones para conectarse a múltiples bases de datos relacionales, entre las que se encuentran: SQLite, PostgreSQL, Oracle y MySQL.
Para la creación de tablas, Django usa la técnica del ORM (Object Relational Mapper), una abstracción del manejo de datos usando OOP.

El ORM es un conjunto de clases que permite interactuar con las bases de datos y definir la estructura de las tablas.
### Modelos por defecto
Existen algunos modelos que django crea por defecto (por ejemplo los relacionados con usuarios, grupos, permisos, etc.)

Cuando se inicia Django con:
```bash
python3 manage.py runserver
```
Aparece un mensaje al final:
```bash
You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.

django-project-1-web-1  | Run 'python manage.py migrate' to apply them.
```
#### Migraciones de modelos por defecto
Al realizar la migración se crearan dichas tablas en la base de datos.
```
python3 manage.py migrate
```
Obteniendo como respuesta
```bash
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK
```
### Modelos nuevos
#### Migraciones de modelos nuevos
Cuando se crea un modelo se la debe indicar que refleje los cambios, los cuales se incluirán dentro de un '0001_initial.py' en la carpeta `posts/migrations`, lo cual permite verificar cómo se ha cambiado la base de datos a lo largo del tiempo.
```bash
python3 manage.py makemigrations
```
Se obtendrá como respuesta
```
Migrations for 'posts':
  posts/migrations/0001_initial.py
    - Create model User
```
Nuevamente, cuando se inicie Django con:
```bash
python3 manage.py runserver
```
Aparece un mensaje al final:
```bash
You have 1 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): posts.

django-project-1-web-1  | Run 'python manage.py migrate' to apply them.
```
#### Migraciones de modelos nuevos
Se realiza el mismo procedimiento que con los modelos por defecto
```
python3 manage.py migrate
```
Obteniendo como respuesta
```bash
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, posts, sessions
Running migrations:
  Applying posts.0001_initial... OK
```
Se creará una tabla nueva en la base de datos llamada `posts_user`
### Estructura final
```
.
├── db.sqlite3
├── posts
:   :   :
:   :   :
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── __init__.py
:   :   :
:   :   :
│   ├── models.py
:   :   :
:   :   :
└── webapp
    ├── settings.py
:   :   :
:   :   :
```
### webapp/settings.py
```py
:   :   :
:   :   :
# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
:   :   :
:   :   :
```
### posts/models.py
```py
'''Posts models.'''

# Django
from django.db import models

# Create your models here.
class User(models.Model):
    '''User model.'''
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=100)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    bio = models.TextField(blank=True)

    birthdate = models.DateField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
```
### posts/migrations/0001_initial.py
```py
# Generated by Django 3.2.8 on 2021-10-10 07:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('password', models.CharField(max_length=100)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('bio', models.TextField(blank=True)),
                ('birthdate', models.DateField(blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
```
### Lecturas recomendadas
- [Django Documentation - Model field reference](https://docs.djangoproject.com/en/2.0/ref/models/fields/)
- [Django Documentation - Settings - Databases](https://docs.djangoproject.com/en/2.0/ref/settings/#databases)






## El ORM de Django
Insertar usuarios a la base de datos en el entorno de pruebas y realizar consultas en el ORM para traer a los admins.

Crear un nuevo campo `is_admin` y el método `__str__` (que devuelva el correo) en el modelo,  insertar usuarios y crear filtros.

### posts/models.py con `is_admin`
```py
'''Posts models.'''

# Django
from django.db import models

# Create your models here.
class User(models.Model):
    '''User model.'''

    email = models.EmailField(unique=True)
    password = models.CharField(max_length=100)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

>   is_admin = models.BooleanField(default=False)

    bio = models.TextField(blank=True)

    birthdate = models.DateField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
```
### posts/migrations/0002_user_is_admin.py
```py
# Generated by Django 3.2.8 on 2021-10-11 02:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_admin',
            field=models.BooleanField(default=False),
        ),
    ]
```
### Interactuar con la BD dese el Shell de Django
#### Insertar un registro
```
python3 manage.py shell
```
Dentro del shell de Django interactuamos con la base de datos
```python
Python 3.8.12 (default, Aug 31 2021, 04:08:54) 
[Clang 12.0.0 (clang-1200.0.32.29)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from posts.models import User
>>> luisca = User.objects.create(
... email='luisca1985@gmail.com',
... password='1234567',
... first_name='Luis Camilo',
... last_name='Jimenez',
... )
>>> luisca.email
'luisca1985@gmail.com'
>>> luisca.id
1
>>> luisca.email = 'luisca1985@hotmail.com'
>>> luisca.save()
>>> luisca.created
datetime.datetime(2021, 10, 11, 2, 40, 49, 538591, tzinfo=<UTC>)
>>> luisca.modified
datetime.datetime(2021, 10, 11, 2, 48, 38, 715981, tzinfo=<UTC>)
```
Otra manera de incluir un usuario es instanciando la clase, incluir los datos y guardando.
```py
>>> sandra=User()
>>> sandra.email = 'sandra@gmail.com'
>>> sandra.first_name = 'Sandra'
>>> sandra.last_name = 'Jimenez'
>>> sandra.password = '7654321'
>>> sandra.is_admin = True
>>> sandra.save()
```
#### Borrar un registro
```py
>>> sandra.delete()
(1, {'posts.User': 1})
```
### Insertar varios registros
```py
>>> from datetime import date
>>> 
>>> users = [
...     {
...         'email': 'cvander@gmail.com',
...         'first_name': 'Christian',
...         'last_name': 'Van der Henst',
...         'password': '1234567',
...         'is_admin': True
...     },
...     {
...         'email': 'freddier@gmail.com',
...         'first_name': 'Freddy',
...         'last_name': 'Vega',
...         'password': '987654321'
...     },
...     {
...         'email': 'yesica@gmail.com',
...         'first_name': 'Yésica',
...         'last_name': 'Cortés',
...         'password': 'qwerty',
...         'birthdate': date(1990, 12,19)
...     },
...     {
...         'email': 'arturo@gmail.com',
...         'first_name': 'Arturo',
...         'last_name': 'Martínez',
...         'password': 'msicomputer',
...         'is_admin': True,
...         'birthdate': date(1981, 11, 6),
...         'bio': "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."...     }
... ]
>>> 
>>> from posts.models import User
>>> 
>>> for user in users:
...     obj = User(**user)
...     obj.save()
...     print(obj.pk, ':', obj.email)
... 
3 : cvander@gmail.com
4 : freddier@gmail.com
5 : yesica@gmailcom
6 : arturo@gmail.com
```

#### Obtener un registro

```py
>>> user = User.objects.get(email='freddier@gmail.com')
>>> user
<User: User object (4)>
>>> type(user)
<class 'posts.models.User'>
>>> user.pk
4
>>> user.first_name
'Freddy'
>>> user.password
'987654321'
```
también se pueden obtener varios usuarios con un filtro
```py
>>> gmail_users = User.objects.filter(email__endswith='@gmail.com')
>>> gmail_users
<QuerySet [<User: User object (3)>, <User: User object (4)>, <User: User object (5)>, <User: User object (6)>]>
```
### posts/models.py con `__str__` como `email`
```py
'''Posts models.'''

# Django
from django.db import models

# Create your models here.
class User(models.Model):
    '''User model.'''

    email = models.EmailField(unique=True)
    password = models.CharField(max_length=100)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    is_admin = models.BooleanField(default=False)

    bio = models.TextField(blank=True)

    birthdate = models.DateField(blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

>   def __str__(self):
>       '''Return email.'''
>       return self.email
```
Corriendo nuevamente el shell de Djengo (`python3 manage.py shell`)
```py
>>> gmail_users = User.objects.filter(email__endswith='@gmail.com')
>>> gmail_users
<QuerySet [<User: cvander@@gmail.com>, <User: freddier@@gmail.com>, <User: yesica@@gmail.com>, <User: arturo@@gmail.com>]>
>>> users = User.objects.all()
>>> users
<QuerySet [<User: luisca1985@hotmail.com>, <User: cvander@gmail.com>, <User: freddier@gmail.com>, <User: yesica@gmail.com>, <User: arturo@gmail.com>]>
```
#### Actualizar varios registros
```py
>>> for u in gmail_users:
...     print(u.email, ':', u.is_admin)
... 
cvander@gmail.com : True
freddier@gmail.com : False
yesica@gmail.com : False
arturo@gmail.com : True
>>> gmail_users.update(is_admin=True)
4
>>> for u in gmail_users:
...     print(u.email, ':', u.is_admin)
... 
cvander@gmail.com : True
freddier@gmail.com : True
yesica@gmail.com : True
arturo@gmail.com : True
```
### Lecturas recomendadas
- [Django Documentation - Making queries](https://docs.djangoproject.com/en/2.0/topics/db/queries/)





## Modelo de usuario de Django
El modelo de usuarios tiene algunas cosas que podrían representar fallas de seguridad en la aplicación, por lo que es necesario utilizar el modelo de usuarios que provee Django y extenderlo con campos adicionales.
### webapp/settings.py
En `webapp/settings.py` se puede encontrar la aplición `django.contrib.auth` que contiene un modelo `auth_user`, y el `SECRET_KEY` que permite codificar el _password_.
```py
:   :   :
:   :   :
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-0nhx=t6r&_&tg-w3$sq&etj-k5o1j$+!98trpov(tccd!6+9n)'
:   :   :
:   :   :
INSTALLED_APPS = [
    # Django apps
    'django.contrib.admin',
>   'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Local apps
    'posts',

]
:   :   :
:   :   :
```
### Crear un usuario desde el shell
```py
Python 3.8.12 (default, Aug 31 2021, 04:08:54) 
[Clang 12.0.0 (clang-1200.0.32.29)] on darwin
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from django.contrib.auth.models import User
>>> u = User.objects.create_user(username='Luis', password='admin123')
>>> u
<User: Luis>
>>> u.pk
1
>>> u.username
'Luis'
>>> u.password
'pbkdf2_sha256$260000$y3PURNrfok1hjX7pHQpIVl$MTtFCiIBg0qCu30lSbm7AR1rV0g+kq8Hy+3M2K2laCw='
```
### Crear un súper usuario
```shell
python3 manage.py createsuperuser
```
Y se deben ingresar algunos datos para registra el súper user:
```shell
Username (leave blank to use 'luiscamilojimenezalvarez'): luisca
Email address: luisca1985@gmail.com
Password: 
Password (again): 
Superuser created successfully.
```
Ahora se creará un nuevo usuario

### Incluir admin a las URLs
#### webapp/ulrs.py
```py
'''Instagram URLs module.'''
# Django
> from django.contrib import admin
from django.urls import path

from webapp import views as local_views
from posts import views as posts_views

urlpatterns = [
>   path('admin/', admin.site.urls),
    path('hello-world', local_views.hello_world),
    path('sorted',local_views.sort_numbers),
    path('hi/<str:name>/<int:age>/', local_views.say_hi),

    path('posts/', posts_views.list_posts),
]
```
#### Ingresar al admin
##### URL
```http
localhost:8000/admin
```
##### Login Admin
![django admin](readmepics/django_admin.png)
##### Admin Site
![django admin](readmepics/django_admin_site2.png)
##### Admin Users
![django admin](readmepics/django_admin_users2.png)
##### Change User
![django admin](readmepics/django_admin_change_user2.png)
#### Repositorio Github de User de Django
```http
https://github.com/django/django/blob/main/django/contrib/auth/models.py
```
##### Class User en models.py
La clase `User` hereda de la clase `AbstractUser`, la cual contiene los campos:
```py
:   :   :
:   :   :
class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
    Username and password are required. Other fields are optional.
    """
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=150, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class User(AbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.
    Username and password are required. Other fields are optional.
    """
    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
:   :   :
:   :   :
```
### Eliminar modelo User
Se elimina el modelo User creado dentro de `posts/models.py`, para utilizar el User que viene por defecto en Django, al igual que la base de datos `db.sqlite3`, y los archivos de `migration` que contienen la información de User.
```
.
├── db.sqlite3
├── posts
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── 0002_user_is_admin.py
│   │   └── __init__.py
│   ├── models.py
:   :   :
:   :   :
```
### Migrar nuevamente
Migrar nuevamente para incluir el modelo de usuario de Django
```bash
% python manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK
```
### posts/models.py
```py
'''Posts models.'''

# Django
from django.db import models

# Create your models here.

```
### Lecturas recomendadas
- [Django Documentation - Customizing authentication in Django - Extending the existing User model](https://docs.djangoproject.com/en/3.2/topics/auth/customizing/#extending-the-existing-user-model)




## Usuarios Personalizados
Las opciones que Django propone para implementar Usuarios personalizados son:
- Usando el Modelo proxy
- Extendiendo la clase abstracta de Usuario existente
La opción OneToOneField restringe la posibilidad de tener perfiles duplicados.

Django no guarda archivos de imagen en la base de datos sino la referencia de su ubicación.
### Crear nueva app `users`
```bash
python3 manage.py startapp users
```
Se crea
```bash
.
:   :   :
:   :   :
├── users
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── migrations
│   │   └── __init__.py
│   ├── models.py
│   ├── tests.py
│   └── views.py
:   :   :
:   :   :
```
### webapp/settings.py
Se instala el app `users` dentro de `settings.py`
```py
:   :   :
:   :   :
INSTALLED_APPS = [
    # Django apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Local apps
    'posts',
>   'users',

]
:   :   :
:   :   :
```

### users/models.py
```py
"""Users models."""

# Django
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Profile(models.Model):
    """Profile model.
    Proxy model that extends the base data with other information.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    website = models.URLField(max_length=200, blank=True)
    biography = models.TextField(blank=True)
    phone_number = models.CharField(max_length=20, blank=True)

    picture = models.ImageField(
        upload_to='users/pictures',
        blank=True,
        null=True
    )

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return username"""
        return self.user.username
```
### Migrar a la base de datos
#### Realizar nuevas migraciones
```bash
python3 manage.py makemigrations
```
Obteniendo como respuesta (si se ha intalado la librería Pillow previmente).
```bash
Migrations for 'users':
  users/migrations/0001_initial.py
    - Create model Profile
```
#### Aplicar las migraciones
```bash
python3 manage.py migrate
```
Obteniendo como respuesta
```bash
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions, users
Running migrations:
  Applying users.0001_initial... OK
```
#### Migrar modelo con campos `ImageField` sin librerìa Pillow
Si el modelo cuenta con campos `ImageField` y no se ha instalado la librería Pillow se obteniendo como respuesta un error:
```bash
SystemCheckError: System check identified some issues:

ERRORS:
users.Profile.picture: (fields.E210) Cannot use ImageField because Pillow is not installed.
```

### Instalar la librería Pillow
#### requirements.txt
```txt
Django
remote-pdb
> Pillow
```
#### Instalar en el entorno virtual
Y se instala el paquete en el entorno virtual
```bash
pip3 install -r requirements.txt
```
#### Generar la imagen en docker
```bash
docker-compose up --build -d
```
### Lecturas recomendadas
- [Django Documentation - Customizing athentication in Django - Extending the existing User model](https://docs.djangoproject.com/en/dev/topics/auth/customizing/#extending-the-existing-user-model)
- [Django Documentation - Model field reference](https://docs.djangoproject.com/en/2.0/ref/models/fields/)
- [Github/django/../models.py](https://github.com/django/django/blob/main/django/contrib/auth/models.py)




## Registrar Usuario Customizado en Admin de Django
Para registar el perfil customizado, junto con el modelo extendido de Usuario, en el admin de Django para poder manejarlo desde la aplicación.

Esto puede hacerse de dos formas: 
- con admin.site.register(Profile)
- creando una nueva clase que herede de Admin.ModelAdmin.
### admin.site.register(Profile)
#### users/admin.py
```py
"""User admin classes"""
# Django
from django.contrib import admin

# Models
from users.models import Profile

# Register your models here.
admin.site.register(Profile)
```
#### Admin con Profiles
![Admin con profile](readmepics/django_admin_users_with_profile.png)
#### Add Profile
![Add profile](readmepics/django_admin_add_profile2.png)
#### Profile Added
![Add profile](readmepics/django_admin_profile_added.png)

### Clase que hereda de Admin.ModelAdmin
```py
"""User admin classes"""
# Django
from django.contrib import admin

# Models
from users.models import Profile

# Register your models here.
# admin.site.register(Profile)

@admin.register(Profile)
class ProfiledAdmin(admin.ModelAdmin):
    """Profile admin."""
    list_display = ('pk', 'user', 'phone_number', 'website', 'picture') # Se muestran los campos sin necesidad de ir al detalle
    list_display_links = ('pk', 'user') # Envía al detalle
    list_editable = ('phone_number', 'website', 'picture') # Se pueden editar sin necesidad de ir al detalle
    search_fields = (
        'user__email',
        'user__username' 
        'user__first_name', 
        'user__last_name', 
        'phone_number'
    ) # Los campos por los que deseamos buscar
    list_filter = (
        'created',
        'modified',
        'user__is_active',
        'user__is_staff'
        ) # Define los filtros de los datos
```
#### Admin con Profiles Mejorado
![Admin con profile](readmepics/django_admin_profiles_improved.png)
### Lecturas recomendadas
- [Django Documentation - Model Admin Options](https://docs.djangoproject.com/en/2.0/ref/contrib/admin/#modeladmin-options)





## Modificar Dash Board de Administración
Se edita el detalle para que sea igual de complejo que el detalle de Usuario y se le agregan los datos del perfil para no tener que estar cambiando de urls. 

Se usa fieldsets y admin.StackedInline.

Para agregar los campos de un modelo de perfil a la página del usuario en el administrador, se debe definir un InlineModelAdmin (usaremos un StackedInline) en el admin.py de la aplicación y agregárselo a la clase UserAdmin que está registrada con la clase `User`.

### users/admin.py
```py
"""User admin classes"""
# Django
> from django.contrib.auth.models import User
> from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin

# Models
from users.models import Profile

# Register your models here.
# admin.site.register(Profile)

@admin.register(Profile)
class ProfiledAdmin(admin.ModelAdmin):
:   :   :
:   :   :
>   fieldsets = (
>       ('Profile', {
>           'fields': (
>               ('user','picture'),),
>       }),
>       ('Extra info',{
>           'fields': (
>               ('website', 'phone_number'),
>               ('biography')
>           )
>       }),
>       ('Metadata',{
>           'fields': (('created', 'modified'),),
>       })
>   )

>   readonly_fields = ('created', 'modified') # Son los campos que no se pueden modificar

> # Definir un descriptor de administrador inline para el modelo de Profiel que actúa similar a un singleton
> class ProfileInline(admin.StackedInline):
>   """Profile in-lin admin for users"""

>   model = Profile
>   can_delete = False
>   verbose_name_plural = 'profiles'

> # Definir un nuevo administrador de usuario
> class UserAdmin(BaseUserAdmin):
>   """Add profile admin to base user admin"""
>   inlines = (ProfileInline, )
>   list_display = (
>       'username',
>       'email',
>       'first_name',
>       'last_name',
>       'is_active',
>       'is_staff'
>   )

> # Volver a registrar UserAdmin
> admin.site.unregister(User)
> admin.site.register(User, UserAdmin)
```
##### Admin con Profile incluído en User
![Admin con profile](readmepics/django_admin_user_with_profile.png)
##### Admin con Profile incluído en User información faltante
![Admin con profile](readmepics/django_admin_user_with_profile_completed.png)
##### Admin con Profile incluyendo los campos de `list_display`
![Admin con profile](readmepics/django_admin_users_with_profile_complemented.png)
### Lecturas recomendadas
- [Django Documentation - The Django admin site](https://docs.djangoproject.com/en/2.0/ref/contrib/admin/)
- [Django Documentation - Customizing athentication in Django - Extending the existing User model](https://docs.djangoproject.com/en/dev/topics/auth/customizing/#extending-the-existing-user-model)




## Modelo de Post
Para reflejar los cambios en la base de datos, siempre que se crea o se edita un modelo debemos cancelar el server, ejecutar makemigrations, migrate y luego de nuevo volver a correr el servidor con runserver.

Con respecto a las imágenes, Django por defecto no está hecho para servir la media, pero editando las urls logramos que se puedan mostrar. Para servir archivos de media, usamos MEDIA_ROOT y MEDIA_URLS.

### posts/models.py
```python
'''Posts models.'''

# Django
from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Post(models.Model):
    """Post model."""
    user = models.ForeignKey(User, on_delete=models.CASCADE) # Llave foránea con User
    profile = models.ForeignKey('users.Profile', on_delete=models.CASCADE)

    title =  models.CharField(max_length=255)
    photo = models.ImageField(upload_to='posts/photos')

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return title y username."""
        return '{} by @{}'.format(self.title, self.user.username)
```

### Migrar los datos del nuevo modelo
```bash
% python3 manage.py makemigrations

Migrations for 'posts':
  posts/migrations/0001_initial.py
    - Create model Post

% python3 manage.py migrate 

Operations to perform:
  Apply all migrations: admin, auth, contenttypes, posts, sessions, users
Running migrations:
  Applying posts.0001_initial... OK
```
Volver a correr el servidor
```bash
python3 manage.py runserver
```
O reiniciar docker
```bash
docker-compose restart
```
### Agregar Post al Admin
#### posts/admin.py
```py
from posts.models import Post

# Register your models here.
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('pk','title', 'user','photo','created','modified')
    list_display_links = ('pk',)
    list_editable = ('photo',)
    list_filter = (
        'created',
        'modified'
        )
```
#### Admin con Posts
![Admin con profile](readmepics/django_admin_posts.png)

### Lecturas recomendadas
- [Django Documentation - Model field reference - ForeignKey](https://docs.djangoproject.com/en/2.0/ref/models/fields/#foreignkey)




## Acceder a imágenes guardadas
### webapp/settings.py
```py
:   :   :
:   :   :
# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

> MEDIA_ROOT = BASE_DIR / 'media'
> MEDIA_URL = '/media/'
```
### webapp/urls.py
```py
# Django
from django.contrib import admin
> from django.conf.urls.static import static
> from django.conf import settings
from django.urls import path

from webapp import views as local_views
from posts import views as posts_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello-world', local_views.hello_world),
    path('sorted',local_views.sort_numbers),
    path('hi/<str:name>/<int:age>/', local_views.say_hi),

    path('posts/', posts_views.list_posts),
> ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
### Lecturas recomendadas
- [Django Documentation - Settings - MEDIA_ROOT](https://docs.djangoproject.com/en/2.0/ref/settings/#media-root)




## Templates y archivos estáticos
Los templates quedarán definidos en un nuevo folder que llamaremos /templates/.

El concepto de archivos estáticos en Django, son archivos que se usan a través de la aplicación para pintar los datos. Pueden ser archivos de imagen, audio y video, o archivos css y scripts js.

Para servir archivos estáticos, nos apoyamos en STATIC_ROOT y STATIC_URLS.

### Desarrollo
- Se crea un folder templates
- Se ajusta la variable `TEMPLATES` en `settings.py`, para indicar que un folder `templates` va a contener otros templates.
- Se crea dentro de `templates` dos folders: 1) `users` y 2) `posts`, para mantenerlos organizados
- Se actualizan los `views` con los nuevos paths de los templates.
- Se mueven los templates a la nueva carpeta template.
- Sreamos un template base que tiene el marco de lo que tendrán todas las páginas, dentro de templates ya que va a ser compartido por todas la aplicaciones. Se llamará `base.html`.
- Creamos un template para la barra de navegación `nav.html` en templates ya que también será compartido.
- Creamos otro `base.html` sólo par a users, ya que tienen un marco diferente a las demás aplicaciones.
- Se debe ajustar los archivos estáticos.
- Se crea un folder `static`.
- Se crea `css` e `img` dentro de `static`. Y se incluyen los archivos.
- Se debe indicar que se cuenta con un folder con archivos estáticos.
- Se incluye dentro de `settings` las direcciones de los archivos estáticos:
    - `STATIC_DIRS` - Directorios donde están los archivos estáticos.
    - `STATIC_FINDERS` - Métodos para encontrar los archivos estáticos. 
### carpeta tempates y static
```bash
.
├── static
│   ├── css
│   │   ├── bootstrap.min.css
│   │   └── main.css
│   └── img
│       ├── default-profile.png
│       └── instagram.png
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── posts
│   │   └── feed.html
│   └── users
│       └── base.html
:   :   :
:   :   :
```
### webapp/settings.py
```py
:   :   :
:   :   :
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
>       'DIRS': [
>           BASE_DIR / 'templates'
>       ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

:   :   :
:   :   :

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'
> STATICFILES_DIRS = (
>   BASE_DIR / 'static',
> )
> STATICFILES_FINDERS = [
>   'django.contrib.staticfiles.finders.FileSystemFinder',
>   'django.contrib.staticfiles.finders.AppDirectoriesFinder',
> ]
:   :   :
:   :   :
```
### posts/views.py
```py
:   :   :
:   :   :
# Create your views here.
def list_posts(request):
    """List existing posts."""
>   return render(request, 'posts/feed.html',{'posts': posts})
```
### templates/posts/feeds.html
```html
{% extends "base.html" %}

{% block head_content %}
<title>Instagram feed</title>
{% endblock head_content %}

{% block container %}
    <div class="row">
        {% for post in posts %}
            <div class="col-lg-4 offset-lg-4">
                <div class="media">
                    <img class="mr-3 rounded-circle" src="{{ post.user.picture }}" alt="{{ post.user.name }}">
                    <div class="media-body">
                        <h5 class="mt-0">{{ post.user.name }}</h5>
                        {{ post.timestamp }}
                    </div>
                </div>
                <img class="img-fluid mt-3 border rounded" src="{{ post.photo }}" alt="{{ post.title }}">
                <h6 class="ml-1 mt-1">{{ post.title }}</h6>
            </div>
        {% endfor %}
    </div>
{% endblock container %}
```
Inyectamos el `feed.html` en `base.html`.

Dentro de `feed.html` se quita el código que ya está en `base.html`, y se incluye un `extends`, para exteder a `base.html`.

Le indicamos dentro de `feed.html` los `block` donde irá el contenido.
    - `head_content`
    - `container`

### templates/base.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {% block head_content %}{% endblock %}


    {% load static %}
    <link rel="stylesheet" href="{% static 'css/bootstrap.min.css' %}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" crossorigin="anonymous" />
    <link rel="stylesheet" href="{% static 'css/main.css' %}" />

</head>
<body>

    {% include "nav.html" %}

    <div class="container mt-5">
        {% block container %}
        {% endblock%}
    </div>

</body>
</html>
```
Los `block` definen un espacio para cuando el template sea utilizado, se pueda escribir dentro de este espacio.

Se carga un tag que se llama `static`, dendo de `head`, el cual generará la ruta de archivos estáticos de manera dinámica, y después buscar los STATICFILES_FINDERS que se definieron previamente.
### templates/nav.html
```html
{% load static %}
<nav class="navbar navbar-expand-lg fixed-top" id="main-navbar">
    <div class="container">

        <a class="navbar-brand pr-5" style="border-right: 1px solid #efefef;" href="">
            <img src="{% static "img/instagram.png" %}" height="45" class="d-inline-block align-top"/>
        </a>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item">
                    <a href="">
                        <img src="{% static 'img/default-profile.png' %}" height="35" class="d-inline-block align-top rounded-circle"/>
                    </a>
                </li>

                <li class="nav-item nav-icon">
                    <a href="">
                        <i class="fas fa-plus"></i>
                    </a>
                </li>

                <li class="nav-item nav-icon">
                    <a href="">
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
```
En la barra de navegación `nav.html` en la imagen incluye un tag `static` para:
- `img/instagram.png`
- `img/default-profile.png`

### templates/users/base.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {% block head_content %}{% endblock %}

    {% load static %}
    <link rel="stylesheet" href="{% static 'css/main.css' %}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" crossorigin="anonymous" />
    <link rel="stylesheet" href="{% static 'css/main.css' %}" />

</head>
<body class="h-100">

    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-sm-12 col-md-5 col-lg-5 pt-2 pl-5 pr-5 pb-5" id="auth-container">

                <img src="{% static "img/instagram.png" %}" class="img-fluid rounded mx-auto d-block pb-4" style="max-width: 60%;">

                {% block container %}{% endblock%}

            </div>
        </div>
    </div>

</body>
</html>
```
### Lecturas recomendadas
- [Django Documentation - Settings - STATIC_ROOT](https://docs.djangoproject.com/en/2.0/ref/settings/#static-files)
- [Django Documentation - Built-in template tags and filters - extends](https://docs.djangoproject.com/en/2.0/ref/templates/builtins/#extends)
- [Django Documentation - Built-in template tags and filters - include](https://docs.djangoproject.com/en/2.0/ref/templates/builtins/#include)
- [Django Documentation - Built-in template tags and filters - static](https://docs.djangoproject.com/en/2.0/ref/templates/builtins/#static)




## Login
Vamos a asegurarnos que para acceder a la aplicación tengamos una cuenta de usuario. Para esto vamos a ver el proceso de autenticación que django nos ofrece para crear la página de login.

Se busca que no se puede acceder a la página, si no se ha autenticado previamente. Por lo cual se necesita hacer un login.

### webapp/setting.py
```py
:   :   :
:   :   :
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
:   :   :
:   :   :
MEDIA_ROOT = BASE_DIR / 'media'
MEDIA_URL = '/media/'

> LOGIN_URL = '/users/login/'
```
El middleware `SessionMiddleware` en `MIDDLEWARE` de `settings.py`, se encarga de validar la sesión de un usuario.

`SessionMiddleware` provee la propiedad `request.user.is_authenticated` dentro de las vistas, para verificar si el usuario está autenticado.

El decorador `@login_required` requiere configurar `LOGIN_URL` en `webapp/settings.py`

### webapp/urls.py
```py
# Django
> from os import name
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path

from webapp import views as local_views
from posts import views as posts_views
> from users import views as users_views

urlpatterns = [
    path('admin/', admin.site.urls),
>   path('hello-world', local_views.hello_world,name='hello_world'),
>   path('sorted',local_views.sort_numbers,name='sort'),
>   path('hi/<str:name>/<int:age>/', local_views.say_hi, name='hi'),

>   path('posts/', posts_views.list_posts,name='feed'),

>    path('users/login/', users_views.login_view, name='login')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
Dentro de `urls.py` se incluye la url para `login`, la cual se dirige a `views.py` de `users`, donde estará ubicada la función `login_view`.

Se le agrega a las funciones `path` dentro de `urls.py` un agurmento `name`.

### users/views.py
```py
"""Users views."""

# Django
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render

# Create your views here.

def login_view(request):
    """Login view."""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('feed')
        else:
            return render(request, 'users/login.html', {'error': 'Invalid username or password'})

    return render(request, 'users/login.html')
```
`django.contrib.auth` tiene las funciones `authenticate` y `login` con las que se puede atenticar.

Se crea la función `login_view` al que llamará a un temmplate `login.html` que se encuentra en `templates/users`.

Dentro de `view.py` se debe traer la petición `POST`, y se debe verifcar que `request.method` sea un `POST`.
### templates/users/login.html
```html
{% extends 'users/base.html' %}

{% block head_content %}
    <title>Instagram sign in</title>
{% endblock head_content %}

{% block container %}

    {% if error %}
        <p style="color:red;">{{ error }}</p>
    {% endif %}

    <form method="POST" action='{% url "login" %}'>
        {% csrf_token %}

        <input type="text" placeholder="Username" name="username">
        <input type="password" placeholder="Password" name="password">

        <button type="submit">Sing in!</button>
    </form>

{% endblock container %}
```
`login.html` se extenderá del archivo `base.html` ubicado en `temaplate/users`.

Dentro del template se utiliza un formulario con `action='{% url "login" %}` el cual llama al `path` en `urls.py` con parámetro `name=login`. Lo que ocurre es que el sitema automáticamente le asigna la url `users/login/` y si el día de mañana se cambiara dicha url, el sistema la acutalizaría automáticamente.

Se incluye dentro del fomulario un `csrf_token` para validar que se haya hecho un `GET` previamente, así validar que se esté haciendo el `POST` desde la misma página y no sea alguien externo a la páfina. Django hace esta validación por temas de seguridad.

Se realiza un condicional en `login.html` para verificar si existe un error, y en tal caso se despliega un mensaje indicando que exite un `error`.
### posts/views.py
```py
'''Posts views.'''
# Django
> from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Utilities
from datetime import datetime

:   :   :
:   :   :

# Create your views here.
>
@login_required
def list_posts(request):
    """List existing posts."""
   return render(request, 'posts/feed.html',{'posts': posts})
```
Se verifica que no se pueda ingresar a `feed` si no existen posts. Por lo cual se realiza un averificación en el `views.py` de `posts`, con el decorador `@login_required`, para lo cual se debe configurar `LOGIN_URL` en `webapp/settings.py`

### Lecturas recomendadas
- [Django Documentation - Using the Django authentication system - Authentication in Web requests](https://docs.djangoproject.com/en/2.0/topics/auth/default/#authentication-in-web-requests)
- [Django Documentation - Using the Django authentication system - The login_required decorator](https://docs.djangoproject.com/en/2.0/topics/auth/default/#the-login-required-decorator)




## Ajuste de estilos del login
Incorporamos algo de estilos al formulario de Login.
### templates/users/login.html
```html
{% extends 'users/base.html' %}

{% block head_content %}
    <title>Instagram sign in</title>
{% endblock head_content %}

{% block container %}

    {% if error %}
        <p class="alert alert-danger">{{ error }}</p>
    {% endif %}

    <form method="POST" action='{% url "login" %}'>
        {% csrf_token %}
>       <div class="form-group">
>           <input type="text" class="form-control" placeholder="Username" name="username">
>       </div>
>       <div class="form-group">
>           <input type="password" class="form-control" placeholder="Password" name="password">
>       </div>
>       <button class="btn btn-primary btn-block mt-5" type="submit">Sing in!</button>
    </form>

{% endblock container %}
```
- Se agrega estilo del login un párrafo de clase `alert alert-danger`, para el `{{ error }}`.
- Se incluye un formulario `form-group` con un input `form-control`, para el `username` y el `password`.
- Se ajusta la clase del `submit`.




## Logout
Completaremos el flujo de autenticación del usuario agregando la funcionalidad de Logout.
### users/views.py
```py
"""Users views."""

# Django
> from django.contrib.auth import authenticate, login, logout
> from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

# Create your views here.

def login_view(request):
    """Login view."""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('feed')
        else:
            return render(request, 'users/login.html', {'error': 'Invalid username or password'})

    return render(request, 'users/login.html')

> @login_required
> def logout_view(request):
>   """Logout a user."""
>   logout(request)
>   return redirect('login')
```
- Se crea la vista del `logout`.
- Se debe verificar que no se realize un `logout` de una sesión inexistente, para lo cual se verifica con el decorador `login_required`.
### webapp/urls.py
```py
from webapp import views as local_views
from posts import views as posts_views
from users import views as users_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello-world', local_views.hello_world,name='hello_world'),
    path('sorted',local_views.sort_numbers,name='sort'),
    path('hi/<str:name>/<int:age>/', local_views.say_hi, name='hi'),

    path('posts/', posts_views.list_posts,name='feed'),

    path('users/login/', users_views.login_view, name='login'),
>   path('users/logout/', users_views.logout_view, name='logout'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
- Se incluye la vista del `logout` en `urls.py`.
### templates/nav.html
```html
{% load static %}
<nav class="navbar navbar-expand-lg fixed-top" id="main-navbar">
    <div class="container">

        <a class="navbar-brand pr-5" style="border-right: 1px solid #efefef;" href="">
            <img src="{% static "img/instagram.png" %}" height="45" class="d-inline-block align-top"/>
        </a>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
:   :   :
:   :   :

                <li class="nav-item nav-icon">
                    <a href="{% url "logout" %}">
>                       <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
```
- Incluímos en la barra de navegación el `logout`
### Lecturas recomendadas
- [Django Documentation - Using the Django authentication system - How to log a user out](https://docs.djangoproject.com/en/2.0/topics/auth/default/#how-to-log-a-user-out)




## Signup
Crearemos el Registro de usuario a partir de la clase perfil, por lo que usaremos un formulario personalizado. Definiremos un nuevo Template para el formulario. Dejaremos que el browser se encargue de las validaciones generales. Sólo validaremos en python la coincidencia entre password y confirmación del password. Incluiremos una validación con try/catch para evitar que se dupliquen usuarios con mismo nombre.

### templates/users/signup.html
```
.
:   :   :
:   :   :
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── posts
│   │   └── feed.html
│   └── users
│       ├── base.html
│       ├── login.html
│ >     └── signup.html
:   :   :
:   :   :
```
```html
{% extends 'users/base.html' %}

{% block head_content %}
    <title>Instagram sign up</title>
{% endblock head_content %}

{% block container %}

    {% if error %}
        <p class="alert alert-danger">{{ error }}</p>
    {% endif %}


    <form method="POST" action='{% url "signup" %}'>
        {% csrf_token %}
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Username" name="username" required="true">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" name="passwd" required="true">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password confirmation" name="passwd_confirmation" required="true">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="First name" name="first_name" required="true">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Last name" name="last_name" required="true">
        </div>
        <div class="form-group">
            <input type="email" class="form-control" placeholder="Email address" name="email" required="true">
        </div>

        <button class="btn btn-primary btn-block mt-5" type="submit">Register!</button>
    </form>

{% endblock container %}
```
- Se crea el template de `signup.html`
- Siempre que se agregue un formulario se debe incluir `csrf_token` ([cross-site request forgery](https://es.wikipedia.org/wiki/Cross-site_request_forgery)).
- Se incluye el mensaje error.
### users/views.py
```py
"""Users views."""

# Django
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

# Exceptions
> from django.db.utils import IntegrityError

# Models
> from django.contrib.auth.models import User
> from users.models import Profile

# Create your views here.

def login_view(request):
    """Login view."""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('feed')
        else:
            return render(request, 'users/login.html', {'error': 'Invalid username or password'})

    return render(request, 'users/login.html')

> def signup_view(request):
>   """Sign up view."""
>   if request.method == 'POST':
>       username = request.POST['username']
>       passwd = request.POST['passwd']
>       passwd_confirmation = request.POST['passwd_confirmation']

>       if passwd != passwd_confirmation:
>           return render(request,'users/signup.html', {'error': 'Password confirmation does not match'})

>       try:
>           user = User.objects.create_user(username=username, password=passwd)
>       except IntegrityError:
>           return render(request,'users/signup.html', {'error': 'Username is already in user'})
               
>       user.first_name = request.POST['first_name']
>       user.last_name = request.POST['last_name']
>       user.email = request.POST['email']
>       user.save()
        
>       profile = Profile(user=user)
>       profile.save()
>       return redirect('login')
        
>   return render(request, 'users/signup.html')

@login_required
def logout_view(request):
    """Logout a user."""
    logout(request)
    return redirect('login')
```
- Se crea `singnup` dentro de `views.py`.
- Se verifica que `request.method` sea un `POST`.
- Se confirma que el `password` sea igual en ambos campos.
- Se crean los `User`y `Profile` con los campos y se guardan.
- Se crea un `try` para que despliegue un diálogo cuando el usuario ya se encuentre en uso, verificando con `IntegrityError` que se trata de este tipo error.
### users/views.py
```py
:   :   :
:   :   :
urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello-world', local_views.hello_world,name='hello_world'),
    path('sorted',local_views.sort_numbers,name='sort'),
    path('hi/<str:name>/<int:age>/', local_views.say_hi, name='hi'),

    path('posts/', posts_views.list_posts,name='feed'),

    path('users/login/', users_views.login_view, name='login'),
    path('users/logout/', users_views.logout_view, name='logout'),
>   path('users/signup/', users_views.signup_view, name='signup'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
- Se crea `singnup` dentro de `urls.py`.
### Lecturas recomendadas
- [Django Documentation - Using the Django authentication system - Creating users](https://docs.djangoproject.com/en/2.0/topics/auth/default/#creating-users)




## Middleware
Un middleware en Django es una serie de hooks y una API de bajo nivel que nos permiten modificar el objeto `request` antes de que llegue a la vista y `response` antes de que salga de la vista.

Django dispone de los siguientes middlewares por defecto:

- SecurityMiddleware
- SessionMiddleware
- CommonMiddleware
- CsrfViewMiddleware
- AuthenticationMiddleware
- MessageMiddleware
- XFrameOptionsMiddleware

Crearemos un middleware para redireccionar al usuario al perfil para que actualice su información cuando no haya definido aún biografía o avatar.

### templates/update_profile.html
```
.
:   :   :
:   :   :
└── templates
    ├── base.html
    ├── nav.html
    ├── posts
    │   └── feed.html
    └── users
        ├── base.html
        ├── login.html
        ├── signup.html
        └── update_profile.html
```
```html
{% extends 'base.html' %}

{% block head_content %}
    <title>@{{ request.user.username }}  | Update profile</title>

{% endblock head_content %}

{% block container %}
    <h1 class="mt-5">@{{ request.user.username }}</h1>

{% endblock container %}
```
- Se crea el template `update_profile.html`, que se extiende de `base.html`.
- Se accede a los datos de `request.user.username`, el cual proviene de `AuthenticationMiddleware` ubicado en `settings.py`, el cual permite acceder a la variable `user` desde cualquier template, sin necesidad de enviar el dato.
### webapp/middleware.py
```
.
:   :   :
:   :   :
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── posts
│   │   └── feed.html
│   └── users
│       ├── base.html
│       ├── login.html
│       ├── signup.html
│       └── update_profile.html
└── webapp
    ├── __init__.py
    ├── asgi.py
    ├── middleware.py
    ├── settings.py
    ├── urls.py
    ├── views.py
    └── wsgi.py
```

```py
"""Instagram middleware catalog."""

# Django
from django.shortcuts import redirect
from django.urls import reverse

class ProfileCompletionMiddleware:
    """Profile completion middleware
    
    Ensure every user that is interactin with the platform
    have their profile picture and biography.
    """

    def __init__(self, get_response):
        """Middleware initialization"""
        self.get_response = get_response

    def __call__(self, request):
        """Code to be executed for each request before the view is called."""
        if not request.user.is_anonymous:
            profile = request.user.profile
            if not profile.picture or not profile.biography:
                if request.path not in [reverse('update_profile'), reverse('logout')]:
                    return redirect('update_profile')
        
        response = self.get_response(request)
        return response
```
- Se crea el archivo `middleware.py` dentro del proyecto `webapp`.
- Se verificará que si un usuario no tiene la información de `Profile` no podrá utilizar la plataforma.
- Se crea la clase `ProfileCompletionMiddleware` dentro del cual cual se verifica que los campos `picture` y `biography` no se encuentren vaciós, y en caso contrario redirige a `update.profile.html`.
- Se importa `reverse` el cual verifica el cumple la misma función que cumple `{% url "signup" %}` en los templates, pero dentro del un .py, para traer un url a partir de un nombre.

### users/views.py
```py
"""Users views."""

# Django
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

# Exceptions
from django.db.utils import IntegrityError

# Models
from django.contrib.auth.models import User
from users.models import Profile

# Create your views here.

> def update_profile(request):
>  """Update a user's profile view.""" 
>  return render(request, 'users/update_profile.html')

:   :   :
:   :   :
```
- Se crea dentro de `views.py` la vista para actulizar el perfil.
### webapp/urls.py
```py
:   :   :
:   :   :

urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello-world', local_views.hello_world,name='hello_world'),
    path('sorted',local_views.sort_numbers,name='sort'),
    path('hi/<str:name>/<int:age>/', local_views.say_hi, name='hi'),

    path('posts/', posts_views.list_posts,name='feed'),

    path('users/login/', users_views.login_view, name='login'),
    path('users/logout/', users_views.logout_view, name='logout'),
    path('users/signup/', users_views.signup_view, name='signup'),
>   path('users/me/profile/', users_views.update_profile, name='update_profile'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
- Se crea en `urls.py` un opción `update_profile` para poder editar el perfil.
### webapp/settings.py
```py
:   :   :
:   :   :

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

>   'webapp.middleware.ProfileCompletionMiddleware',
]

:   :   :
:   :   :
```
- Se instala el middleware en `settings.py`
### Lecturas recomendadas
- [Django Documentation - Middleware](https://docs.djangoproject.com/en/2.0/topics/http/middleware/)




## Formularios
La clase utilitaria para formularios de Django nos ayuda a resolver mucho del trabajo que se realiza de forma repetitiva. La forma de implementarla es muy similar a la implementación de la clase `models.model`.

Algunas de las clases disponibles en Django al implementar form, son:
- BooleanField
- CharField
- ChoiceField
- TypedChoiceField
- DateField
- DateTimeField
- DecimalField
- EmailField
- FileField
- ImageField"

### Apuntes
### users/forms.py
```
.
├── users
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│>  ├── forms.py
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── __init__.py
│   ├── models.py
│   ├── pictures
│   │   ├── luisca_profile.jpeg
│   │   └── luisca_profile_KSBxm7M.jpeg
│   ├── tests.py
│   └── views.py
:   :   :
:   :   :
```
```py
"""User forms."""

# Django
from django import forms

class ProfileForm(forms.Form):
    """Profile form."""

    website = forms.URLField(max_length=200, required=True)
    biography = forms.CharField(max_length=500, required=False)
    phone_number = forms.CharField(max_length=20, required=False)
    picture = forms.ImageField()
```
- Creamos un módulo `forms.py` para gestionar los formularios.
- Creamos una clase `ProfileForm` para gestionar los formularios del profile.
- Se incluye la validación de los datos dentro de los campos de `ProfileForm`.

### users/views.py
```py
:   :   :
:   :   :
> # Forms
> from users.forms import ProfileForm

# Create your views here.

def update_profile(request):
   """Update a user's profile view."""
>  profile = request.user.profile
   
>  if request.method == 'POST':
>       form = ProfileForm(request.POST, request.FILES)
>       if form.is_valid():
>           data = form.cleaned_data

>           profile.website = data['website']
>           profile.phone_number = data['phone_number']
>           profile.biography = data['biography']
>           profile.picture = data['picture']
>           profile.save()

>           return redirect('update_profile')
>  else:
>      form = ProfileForm()

>  return render(
>      request=request,
>      template_name= 'users/update_profile.html',
>      context={
>          'profile': profile,
>          'user': request.user,
>          'form': form,
>      }
>   )
:   :   :
:   :   :
```
- Se crea un `form` de tipo `ProfileForm` que recibe el `request` y verifica que los campos cumplen las condiciones definidas en `forms.py`.
- Se indica que los archivos se envían con `request.FILES`.
- Se verifica que los datos son válidos con `form.is_valid()`, y se limipian los datos con `form.cleaned_data`.
- Se incluyen los datos dentro de `profile` y se guardan los cambios.
- Se incluye en el contexto `profile` y `user` en la función `update_profile`, para acceder directamente al `user` y no tener que acceder como `request.user`.
- Se incluye `form` en `context`.

### template/users/update_profile.html
```html
{% extends 'base.html' %}
{% load static%}

{% block head_content %}
    <title>@{{ request.user.username }}  | Update profile</title>

{% endblock head_content %}

{% block container %}

>   <div class="row justify-content-md-center">
>       <div class="col-6 p-4" id="profile-box">
>           <form action={% url 'update_profile' %} method="POST" enctype="multipart/form-data">
>               {% csrf_token %}
>
>               {% if form.errors %}
>                   <p class="alert alert-danger">{{ form.errors }}</p>
>               {% endif %}
>               <div class="media">
>                   {% if profile.picture %}
>                       <img src=" {{ profile.picture.url }} " alt="Profile picture" class="rounded-circle" height="50">
>                   {% else %}
>                       <img src="{% static 'img/default-profile.png' %}" alt="Profile picture" class="rounded-circle" height="50">
>                   {% endif %}
>                   
>                   <div class="media-body">
>                       <h5 class="ml-4">@{{ user.username }} | {{ user.get_full_name }}</h5>
>                       <p class="ml-4"><input type="file" name="picture" required="true"></p>
>                   </div>
>               </div>
>               
>               <hr><br>
>
>               <div class="form-group">
>                   <label for="website">Website</label>
>                   <input 
>                       type="url" 
>                       class="form-control" 
>                       name="website" 
>                       placeholder="Website"
>                       value="{{ profile.website }}"
>                   >
>               </div>
>
>               <div class="form-group">
>                   <label for="biography">Biography</label>
>                   <textarea name="biography" id="" cols="30" rows="10" class="form-control">{{ profile.biography }}</textarea>
>               </div>
>
>               <div class="form-group">
>                   <label for="phone_number">Phone number</label>
>                   <input 
>                       type="text" 
>                       class="form-control" 
>                       name="phone_number" 
>                       placeholder="Phone number" 
>                       value="{{ profile.phone_number }}">
>               </div>
>
>               <button type="submit" class="btn btn-primary btn-block mt-5">Update info</button>
>
>           </form>
>       </div>
>   </div>

{% endblock container %}
```
- Se incluyen los campos de `profile` dentro del formulario para acutalizarlos.
- Se le indica al formalario del template que `action` es dirigirse a `update_profile`, que el `method` es `POST` y un `enctype` igual a `multipart/form-data` para indicar que el formulario va a estar enviando diferente tipo de datos.
- Se muestra la foto, primero verificando si la foto existe (`if profile.picture`), si no existe mantiene la imagen por defecto (`default-profile.png`).
    
### webapp/middleware.py
```py
:   :   :
:   :   :
    def __call__(self, request):
        """Code to be executed for each request before the view is called."""
        if not request.user.is_anonymous:
>           if not request.user.is_staff:
                profile = request.user.profile
                if not profile.picture or not profile.biography:
                    if request.path not in [reverse('update_profile'), reverse('logout')]:
                        return redirect('update_profile')
        
        response = self.get_response(request)
        return response
```
- Se genera un condicional en `middleware.py` para que sólo se aplique si el usuario no pertenece al staff (`if not request.user.is_staff`)

### Lecturas recomendadas
- [Django Documentation - Working with forms](https://docs.djangoproject.com/en/2.0/topics/forms/)
- [Django Documentation - Form fields](https://docs.djangoproject.com/en/2.0/ref/forms/fields/)




## Model forms
ModelForm es una manera más sencilla de crear formularios en Django y en el caso de nuestro proyecto, se adapta mucho mejor al modelo que ya tenemos.
Lo usaremos para crear el formulario de posts.

Aprovecharemos para refinar la funcionalidad en el navbar y conectar el feed con los posts.

### webapp/urls.py
```py
:   :   :
:   :   :
urlpatterns = [
    path('admin/', admin.site.urls),
    path('hello-world', local_views.hello_world,name='hello_world'),
    path('sorted',local_views.sort_numbers,name='sort'),
    path('hi/<str:name>/<int:age>/', local_views.say_hi, name='hi'),

>   path('', posts_views.list_posts,name='feed'),
>   path('posts/new/', posts_views.create_post, name='create_post'),

    path('users/login/', users_views.login_view, name='login'),
    path('users/logout/', users_views.logout_view, name='logout'),
    path('users/signup/', users_views.signup_view, name='signup'),
    path('users/me/profile/', users_views.update_profile, name='update_profile'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
- Cambiarmos el `url` de post para que se encuentre en principal (`/`).
- Creamos una `url` nueva `posts/news`, con nombre `create_post` para crear nuevos posts, que e dirige a la vista `posts_views`. 

### posts/views.py
```py
'''Posts views.'''
# Django
from django.contrib.auth.decorators import login_required
> from django.shortcuts import render, redirect

> # Forms
> from posts.forms import PostForm

> # Models
> from posts.models import Post

# Create your views here.
@login_required
def list_posts(request):
    """List existing posts."""
>   posts = Post.objects.all().order_by('created')
    return render(request, 'posts/feed.html',{'posts': posts})

> @login_required
> def create_post(request):
    """Create new post view."""
>   if request.method == 'POST':
>       form = PostForm(request.POST, request.FILES)
>       if form.is_valid():
>           form.save()
>           return redirect('feed')
>   else:
>       form = PostForm()
>   
>   return render(
>       request=request,
>       template_name='posts/new.html',
>       context={
>           'form': form,
>           'user': request.user,
>           'profile': request.user.profile,
>       }
>   )
```
- Se incluye `create_post`, indicándo que se debe estar logueado `@login_required`. 
- Si el `request` es un `POST` obtiene los datos y los verifica con `PostForm`.
- Verifica si los cámpos son válidos con `form.is_valid`.
- Dade que se utiliza `ModelForms` se pueden guardar los valores con `form.save()` sin necesidad de asignarlos, ya que los formularios ya vienen asociados al `model` previamente.
- Se realiza la redirección al `feed`.
- Se renderiza el template, incluyendo el contexto.
- Se elimina la lista `posts`, y se obtiene los posts directamente de la base de datos.
- Se elimina `datetime` ya que no se está utilizando.

### users/views.py
```py
:   :  :
:   :  :
# Create your views here.

@login_required
def update_profile(request):
   """Update a user's profile view."""
   profile = request.user.profile
:   :  :
:   :  :
```
- Se incluye `@login_required` para poder actualizar el perfil.

### posts/forms.py
```
.
├── posts
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── forms.py
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   └──  __init__.py
│   ├── models.py
│   ├── photos
│   │   └── luisca_profile.jpeg
│   ├── tests.py
│   └── views.py
:   :   :
:   :   :
```
```py
"""Post forms."""

# Django
from django import forms

# Models
from posts.models import Post

class PostForm(forms.ModelForm):
    
    class Meta:
        model = Post
        fields = ("user","profile", "title", "photo")
```
- Se crear el modulo `forms.py` en la carpeta `posts`.
- Se importa `forms`, y `Post`.
= Se crea la clase `PostForm` que hereda de `forms.ModelForm`, indicándole que el modelo es `Post` y que los campos son `user`, `profile`, `title` y `photo`.

### templates/posts/new.html
```
.
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── posts
│   │   ├── feed.html
│   │   └── new.html
│   └── users
│       ├── base.html
│       ├── login.html
│       ├── signup.html
│       └── update_profile.html
:   :   :
:   :   :
```
```py
{% extends 'base.html' %}

{% block head_content %}
    <title>Create new post</title>
{% endblock head_content %}

{% block container %}
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-6 pt-3 pb-3" id="profile-box">
                <h4 class="mb-4">Post a new photo!</h4>
 
                <form method="POST" enctype="multipart/form-data">
                    {% csrf_token %}
 
                    {% for error in form.errors %}
                        <div class="alert alert-danger" role="alert">
                             {% for e in error %}
                                {{ e }}
                            {% endfor %}
                        </div>
                    {% endfor %}

                    <input type="hidden" name="user" value="{{ user.pk }}">
                    <input type="hidden" name="profile" value="{{ profile.pk }}">

                    {# Website field #}
                    <div class="form-group">
                        <input 
                            type="text" 
                            name="title" 
                            class="form-control {% if form.title.errors %}is-invalid{% endif %}" 
                            placeholder="Title"
                        >
                        <div class="invalid-feedback">
                            {% for error in form.title.erros %}{{ error }}{% endfor %}
                        </div>
                    </div>

                    {# Photo field #}
                    <div class="form-group">
                        <label for="photo">Choose your photo:</label>
                        <input 
                            type="file" 
                            class="form-control {% if form.photo.errors %}is-invalid{% endif %}" 
                            name="photo" 
                            placeholder="photo" 
                        >

                        <div class="invalid-feedback">
                            {% for error in form.photo.errors %}{{ error }}{% endfor %}
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block mt-5">Publish!</button>
                    
                </form>
            </div>
        </div>
    </div>
{% endblock container %}
```
- Se crea el template `new.html` dentro de `posts`.
- Si el formulario es válido nos redirecciona a `feed`
- Si se envían los campos de manera incorrecta se despliegan mensajes indicando los errores.

### webapp/templates/nav.html
```html
{% load static %}
<nav class="navbar navbar-expand-lg fixed-top" id="main-navbar">
    <div class="container">

        <a class="navbar-brand pr-5" style="border-right: 1px solid #efefef;" href="{% url 'feed' %}">
            <img src="{% static "img/instagram.png" %}" height="45" class="d-inline-block align-top"/>
        </a>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item">
                    <a href="">
>                       {% if request.user.profile.picture %}
>                           <img src="{{ request.user.profile.picture.url }}" height="35" class="d-inline-block align-top rounded-circle"/>
>                       {% else %}
>                           <img src="{% static 'img/default-profile.png' %}" height="35" class="d-inline-block align-top rounded-circle"/>
>                       {% endif %}
                        
                    </a>
                </li>

                <li class="nav-item nav-icon">
>                   <a href="{% url 'create_post' %}">
                        <i class="fas fa-plus"></i>
                    </a>
                </li>

                <li class="nav-item nav-icon">
                    <a href="{% url "logout" %}">
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
```
- El logo de instagram direcciona a `feed`.
- Se incluye la foto si existe `request.user.profile.picture`.
- El símbolo de agregar envía a `create_post`

### webapp/templates/posts/feed.html
```html
{% extends "base.html" %}

{% block head_content %}
<title>Instagram feed</title>
{% endblock head_content %}

{% block container %}
    <div class="container">
        <div class="row">
      
            {% for post in posts %}
                <div class="col-sm-12 col-md-8 offset-md-2 mt-5 p-0 post-container">
                    <div class="media pt-3 pl-3 pb-1">
                        <img 
                            src="{{ post.profile.picture.url }}" 
                            alt="{{ post.user.get_full_name }}"
                            class="mr-3 rounded-circle" 
                            height="35"
                        >
                        <div class="media-body">
                            <p style="margin-top: 5px;">{{ post.user.get_full_name }}</p>
                        </div>
                    </div>

                    <img src="{{ post.photo.url }}" alt="{{ post.title }}" style="width: 100%;">

                    <p class="mt-1 ml-2">
                        <a href="" style="color: #000; font-size: 20px;">
                            <i class="far fa-heart"></i>
                        </a> 30 likes
                    </p>
                    <p class="ml-2 mt-0 mb-2">
                        <b>{{ post.title }}</b> - <small>{{ post.created }}</small>
                    </p>
                </div>
            {% endfor %}
        </div>
    </div>
{% endblock container %}
```
- Se ajusta el `feed` de tal manera que contiene:
    - la foto de perfil de quien realiza el post
    - El nombre completo del usuario
    - La imagen del post
    - Número de likes
    - Título
    - Fecha de creación

### Lecturas recomendadas
- [Django Documentation - Creating forms from models](https://docs.djangoproject.com/en/2.0/topics/forms/modelforms/)




## Validación de Formularios
Para aprender a validar los campos de un formulario vamos a actualizar el registro de usuarios.
Hasta este momento el script de validación del formulario Signup está escrito directamente en la vista, y a pesar de que no genera ningún error, puede convertirse en un problema, así que lo recomendable es separarlo. Crearemos un nuevo form con la clase forms.Form, también vamos a introducir un nuevo concepto relacionado con formularios: los widgets.

Los widgets en Django, son una representación de elementos de HTML que pueden incluir ciertas validaciones. Por default todos los campos son requeridos. Los datos depurados se pueden consultar con `self.cleaned_data['_nombre_del_field_']`

### templates/users/login.html
```html
:   :   :
:   :   :
{% block container %}

    {% if error %}
        <p class="alert alert-danger">{{ error }}</p>
    {% endif %}

    <form method="POST" action='{% url "login" %}'>
        {% csrf_token %}
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Username" name="username">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" name="password">
        </div>
        <button class="btn btn-primary btn-block mt-5" type="submit">Sing in!</button>
    </form>

    <p class="mt-4">Don't have an account yet? <a href="{% url 'signup' %}">Sign up here.</a></p>

{% endblock container %}
```

- Se agrega un enlace para que los usuarios que aún no están registrados vaya a `signup`

### users/forms.py
```py
"""User forms."""

# Django
from django import forms

# Models
from django.contrib.auth.models import User
from users.models import Profile

class SignupForm(forms.Form):
    """Sign up form."""
    username = forms.CharField(min_length=4, max_length=50)
    password = forms.CharField(
        max_length=70,
        widget=forms.PasswordInput()
        )

    password_confirmation = forms.CharField(
        max_length=70,
        widget=forms.PasswordInput()
        )

    first_name = forms.CharField(min_length=2, max_length=50)
    last_name = forms.CharField(min_length=2, max_length=50)

    email = forms.CharField(
        min_length=6, 
        max_length=70,
        widget=forms.EmailInput()
        )

    def clean_username(self):
        """Username must be unique."""
        username = self.cleaned_data['username']
        username_taken = User.objects.filter(username=username).exists()
        if username_taken:
            raise forms.ValidationError('Username is already in use.')
        return username

    def clean(self):
        """Verify password confirmation match."""
        data = super().clean()

        password = data['password']
        password_confirmation = data['password_confirmation']

        if password != password_confirmation:
            raise forms.ValidationError('Password do not match.')

        return data

    def save(self):
        """Create user and profile."""
        data = self.cleaned_data
        data.pop('password_confirmation')

        user = User.objects.create_user(**data)
        profile = Profile(user=user)
        profile.save()


class ProfileForm(forms.Form):
    """Profile form."""

    website = forms.URLField(max_length=200, required=True)
    biography = forms.CharField(max_length=500, required=False)
    phone_number = forms.CharField(max_length=20, required=False)
    picture = forms.ImageField()
```
- Se crea un formulario `SignupForm` para validar los campos de `signup`.
- En `password` se incluye el widget `forms.PasswordInput`, para que el valor sea de tipo password, o que el valor unca se muestre, y se valide la contraseña, entre otras propiedades.
-  En `email` se icluye el widget `forms.EmailInput`, el cual valida el campo de correo.
- Se puede validar un sólo campo, para este caso `username`, utilizando `clean_username`. Se obtiene este dato obteniendo `self.cleaned_data` que contiene los datos que django ya limpió para nosotros.
- Se hace un query a la base de datos utilizado `User.object.filter`, y se utiliza la función `exist` para verificar si existe el usuario, y no extraer el usario, ya que éste último no se encesita. Si el usuario ya existe se envía un error de validación indicando que el usuario ya existe. Si el usuario no existe se retorna el usuario (siempre que se realice una validación, se debe retornar el usuario).
- Se valida el `password` utilizando la función `clean`.
- Se crea la función `save` para guardar los datos dentro de `User` y `Profile`.
- Se utiliza la opción `pop` para que no se incluya `password_confirmation`.

### users/views.py
```py
"""Users views."""

# Django
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render


# Forms
from users.forms import ProfileForm, SignupForm

:   :    :
:   :    :

def signup_view(request):
    """Sign up view."""
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = SignupForm()

    return render(
        request=request,
        template_name='users/signup.html',
        context={'form': form}
    )

```
- Se elimina la información y se remplaza por la validación utilizando `SignupForm` y su función `is_valid`.
- Se eliminan los `imports` realcionados con los modelos y la excepciones.

### templates/users/signup.html
```html
{% extends 'users/base.html' %}

{% block head_content %}
    <title>Instagram sign up</title>
{% endblock head_content %}

{% block container %}

    <form method="POST" action='{% url "signup" %}'>
        {% csrf_token %}

        {{ form.as_p }}

        <button class="btn btn-primary btn-block mt-5" type="submit">Register!</button>
    </form>

{% endblock container %}
```
- Se ajusta el formulario para construirlo de la manera que sugiere django, en forms templates, con `{{ forms.as_p}}`, el cual formatea cada entrada dentro de un `<p></p>`.
### Lecturas recomendadas
- [Django Documentation - Widgets](https://docs.djangoproject.com/en/2.0/ref/forms/widgets/)
- [Django Documentation - Form Fields](https://docs.djangoproject.com/en/2.0/ref/forms/fields/)
- [Django Documentation - Form and field validation](https://docs.djangoproject.com/en/2.0/ref/forms/validation/)




## Class-based views
Veamos de qué forma optimizamos el proceso de creación de nuestras apps de forma que no repitamos código. Para ver cuál es el concepto de class based views.

Las vistas también pueden ser clases, que tienen el objetivo de evitar la repetición de tareas como mostrar los templates, son vistas genéricas que resuelven problemas comunes.

### webapp/urls.py
```py
# Django
from os import name
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include

from posts import views as posts_views
from users import views as users_views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', include(('posts.urls', 'posts'), namespace='posts')),
    path('users/', include(('users.urls', 'users'), namespace='users')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
```
- Se eliminan la urls que ya no se utilizarán.
- Se organizan las urls de tal manera que estén contenidas dentro de la aplicación a las que hacen referencia.
- Se utiliza el `path` donde se indica la primera parte del path asociado a cada aplicación. Adicionalmente, `include` que contiene un tupla con los módulos de las urls de cada aplicación (por ejemplo, `posts.urls`), seguido de la aplicación (por ejemplo, `posts`), y el `namespace` que es nombre que va a definer las urls de la aplicación (por ejemplo, `posts`). 

### webapp/views.py
- Se elimina este archivo ya que no se está utilizando.

### posts/urls.py
```
.
├── posts
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── forms.py
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   └── __init__.py
│   ├── models.py
│   ├── photos
│   │   └── luisca_profile.jpeg
│   ├── tests.py
│   ├── urls.py
│   └── views.py
:   :   :
:   :   :
```
```py
"""Posts URLs."""

# Django
from django.urls import path

# Views
from posts import views

urlpatterns = [
    path(
        route="",
        view=views.list_posts,
        name="feed"
    ),
    path(
        route="posts/new/",
        view=views.create_post,
        name="create"
    ),

    
]
```
- Se incluyen las urls relacionados con `posts`.
- Para cada `path` se define la ruta, la vista, y el nombre.

### users/urls.py
```
.
├── posts
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── forms.py
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   └──  __init__.py
│   ├── models.py
│   ├── photos
│   │   └── luisca_profile.jpeg
│   ├── tests.py
│   ├── urls.py
│   └── views.py
├── users
│   ├── __init__.py
│   ├── admin.py
│   ├── apps.py
│   ├── forms.py
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   └── __init__.py
│   ├── models.py
│   ├── pictures
│   │   ├── luisca_profile.jpeg
│   │   └── luisca_profile_KSBxm7M.jpeg
│   ├── tests.py
│   ├── urls.py
│   └── views.py
:   :   :
:   :   :
```
```py
"""Users URLs."""

# Django
from django.urls import path
from django.views.generic import TemplateView

# Views
from users import views

urlpatterns = [
    
    # Posts
    path(
        route='<str:username>/',
        view=TemplateView.as_view(template_name='users/detail.html'),
        name='detail'
    ),   

    # Management
    path(
        route='users/login/',
        view=views.login_view,
        name='login'
    ),
    path(
        route='users/logout/',
        view=views.logout_view,
        name='logout'
    ),
    path(
        route='users/signup/',
        view=views.signup_view,
        name='signup'
    ),
    path(
        route='users/me/profile/',
        view=views.update_profile,
        name='update_profile'
    ),

    
]
```
- Se incluyen las urls relacionados con `users`.
- Las urls cambiaron de nombre por lo cual se deben ajustar.

### templates/nav.html
```html
{% load static %}
<nav class="navbar navbar-expand-lg fixed-top" id="main-navbar">
    <div class="container">

        <a class="navbar-brand pr-5" style="border-right: 1px solid #efefef;" href="{% url 'posts:feed' %}">
            <img src="{% static "img/instagram.png" %}" height="45" class="d-inline-block align-top"/>
        </a>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item">
                    <a href="">
                        {% if request.user.profile.picture %}
                            <img src="{{ request.user.profile.picture.url }}" height="35" class="d-inline-block align-top rounded-circle"/>
                        {% else %}
                            <img src="{% static 'img/default-profile.png' %}" height="35" class="d-inline-block align-top rounded-circle"/>
                        {% endif %}
                        
                    </a>
                </li>

                <li class="nav-item nav-icon">
                    <a href="{% url 'posts:create
                    ' %}">
                        <i class="fas fa-plus"></i>
                    </a>
                </li>

                <li class="nav-item nav-icon">
                    <a href="{% url "users:logout" %}">
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
```
- Se ajustan los nombres de las urls.

### users/details.html
```
.
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── posts
│   │   ├── feed.html
│   │   └── new.html
│   └── users
│       ├── base.html
│       ├── detail.html
│       ├── login.html
│       ├── signup.html
│       └── update_profile.html
:   :   :
:   :   :
```
```html
{% extends 'base.html' %}

{% block head_content %}
    <title>@{{ request.user.name }} | Instagram</title>
{% endblock head_content %}

{% block container %}
    <div class="container mb-5" style="margin-top: 8em;">
        <div class="row">
            <div class="col-sm-4 d-flex justify-content-center">
                <img src="{{ request.user.profile.picture.url }}" alt="@{{ request.user.username }}" class="rounded-circle" width="150px">
            </div>
            <div class="col-sm-8">
                <h2 style="font-weight: 100;">
                    {{ request.user.username }}
                    {% if request.user == user %}
                        <a 
                            href="{% url 'users:update_profile' %}" 
                            class="ml-5 btn btn-sm btn-outline-info"
                        >
                            Edit profile
                        </a>
                    {% else %}
                        <a 
                            href="" 
                            class="ml-5 btn btn-sm btn-primary"
                        >
                        Follow
                        </a>
                    {% endif %}
                </h2>
                <div class="row mt-2" style="font-size: 1.2em">
                    <div class="col-sm-4"><b>{{ request.user.profile.posts_count }}785</b></div>
                    <div class="col-sm-4"><b>{{ request.user.profile.followers }}1,401</b></div>
                    <div class="col-sm-4"><b>{{ request.user.profile.following }}491</b></div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12">
                        <p>{{ request.user.profile.biography }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock container %}
```
- Se crea el template para el detalle del usuario

### Lecturas recomendadas
- [Django Documentation - Class-based views](https://docs.djangoproject.com/en/2.0/topics/class-based-views/)
- [Django Documentation - Built-in class-based views API](https://docs.djangoproject.com/en/2.0/ref/class-based-views/)
- [Classy Class-Based Views](http://ccbv.co.uk/)






## Protegiendo la vista de perfil, Detail View y Lis View
Vamos a usar Detail View para continuar con el perfil de usuario y usaremos List View para definir la lista de views.

### users/urls.py
````py
"""Users URLs."""

# Django
from django.urls import path

# Views
from users import views

urlpatterns = [
    
    # Posts
    path(
        route='<str:username>/',
```
        view=views.UserDetailView.as_view(),
```
        name='detail'
    ),   

    # Management
    path(
```
        route='login/',
```
        view=views.login_view,
        name='login'
    ),
    path(
```
        route='logout/',
```
        view=views.logout_view,
        name='logout'
    ),
    path(
```
        route='signup/',
```
        view=views.signup_view,
        name='signup'
    ),
    path(
```
        route='me/profile/',
```
        view=views.update_profile,
```
        name='update'
```
    )

    
]
````

- Se extrae `TemplateView`, y se crea externamente, y se llama utilizando `views.UserDetailView.as_view()`.
- Se cambia `update_profile` por `update`.
- Para cada una de las rutas se elimina el `users/` del comienzo, por ejemplo, `users/login/` se cambia por `login/` y así para todas las urls.

### users/views.py
````py
"""Users views."""

# Django
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
```
from django.contrib.auth.mixins import LoginRequiredMixin
```
from django.shortcuts import redirect, render
```
from django.urls import reverse
from django.views.generic import DetailView
```
# Models
from django.contrib.auth.models import User
from posts.models import Post

# Forms
from users.forms import ProfileForm, SignupForm
```
queryset = User.objects.all()
```

# Create your views here.
```
class UserDetailView(LoginRequiredMixin, DetailView):
    """User detail view."""

    template_name = 'users/detail.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'
    queryset = User.objects.all()
    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        """Add user's posts to context"""
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        context['posts'] = Post.objects.filter(user=user).order_by('-created')
        return context
```

@login_required
def update_profile(request):
   """Update a user's profile view."""
   profile = request.user.profile
   
   if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data

            profile.website = data['website']
            profile.phone_number = data['phone_number']
            profile.biography = data['biography']
            profile.picture = data['picture']
            profile.save()

```
            url = reverse('users:detail', kwargs={'username': request.user.username })
            return redirect(url)
```
   else:
       form = ProfileForm()

   return render(
       request=request,
       template_name= 'users/update_profile.html',
       context={
           'profile': profile,
           'user': request.user,
           'form': form,
       }
    )

def login_view(request):
    """Login view."""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
```
            return redirect('posts:feed')
```
        else:
            return render(request, 'users/login.html', {'error': 'Invalid username or password'})

    return render(request, 'users/login.html')

def signup_view(request):
    """Sign up view."""
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
```
            return redirect('users:login')
```
    else:
        form = SignupForm()

    return render(
        request=request,
        template_name='users/signup.html',
        context={'form': form}
    )

@login_required
def logout_view(request):
    """Logout a user."""
    logout(request)
```
    return redirect('users:login')
```
````
- Se crea la clase `UserDetailView` que hereda de `TemplateView`. 
- Se cambia `TemplateView` por `DetailView` ya que es más adecuado para el render de la página de detalle.
- Se incluye el `queryset` requerido por `DetailView`, y se incluyen las variables `slug_field` y `slug_url_kwarg`, con el mismo nombre de la varible del lado de los `urls`, para nuestro caso `username`.
- Se cambia en todos los `redirect()` de tal manera que `update_profile` sea `users:update`, `feed` sea `posts:feed`, `login` sea `users:login`
- Se cambia la redirección dentro de `update_profile`, para que ya no sea a `users:update`, sino a `users:detail`.
- Se contruye la `url` de `redirect` utilizando `reverse`, el cual contiene el argumento `username`.
- Incluimos `context_object_name` para asegurarse que el nombre del objeto que se traiga es `user`.
- Sobreescribimos el método `get_context_data`, para que se agreguen los `posts` del usuario.
- Hacemos qeu `UserDetailView` también herede de `LoginRequiredMixin`

### templates/users/detail.html
````html
{% extends 'base.html' %}

{% block head_content %}
```
    <title>@{{ user.name }} | Instagram</title>
```
{% endblock head_content %}

{% block container %}
    <div class="container mb-5" style="margin-top: 8em;">
        <div class="row">
            <div class="col-sm-4 d-flex justify-content-center">
```
                <img src="{{ user.profile.picture.url }}" 
                alt="@{{ user.username }}" 
                class="rounded-circle" 
                width="150px">
```
            </div>
            <div class="col-sm-8">
                <h2 style="font-weight: 100;">
```
                    {{ user.username }}
                    {% if user == request.user %}
                        <a 
                            href="{% url 'users:update' %}" 
```
                            class="ml-5 btn btn-sm btn-outline-info"
                        >
                            Edit profile
                        </a>
                    {% else %}
                        <a 
                            href="" 
                            class="ml-5 btn btn-sm btn-primary"
                        >
                        Follow
                        </a>
                    {% endif %}
                </h2>
                <div class="row mt-2" style="font-size: 1.2em">
```
                    <div class="col-sm-4"><b>{{ user.profile.posts_count }}785</b></div>
                    <div class="col-sm-4"><b>{{ user.profile.followers }}1,401</b></div>
                    <div class="col-sm-4"><b>{{ user.profile.following }}491</b></div>
```
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12">
```
                        <p>{{ user.profile.biography }}</p>
```
                    </div>
                </div>
            </div>
        </div>
    </div>
```    
    <tr>
        <div class="container" id="user-posts">
            <div class="row mt-3">
                {% for post in posts %}
                    <div class="col-sm-4 pt-5 pb-5 pr-5 pl-5 d-flex justify-content-center align-items-center">
                        <a href="" class="border">
                            <img src="{{ post.photo.url }}" alt="{{ post.title }}" class="img-fluid">
                        </a>
                    </div>
                {% endfor %}
            </div>
        </div>
    </tr>
```
{% endblock container %}
````
- Remplazamos todos los `request.users` por `users`, para que no llame al usuario de la sesión actual, sino el llamado po la vista `DetailView`. 
- Se cambia `update_profile` por `users:update`.
- Se incluyen los posts al final.

### templates/users/update_profile.html
````html
{% extends 'base.html' %}
{% load static%}

{% block head_content %}
    <title>@{{ request.user.username }}  | Update profile</title>

{% endblock head_content %}

{% block container %}

    <div class="row justify-content-md-center">
        <div class="col-6 p-4" id="profile-box">
```
            <form action={% url 'users:update' %} method="POST" enctype="multipart/form-data">
```
                {% csrf_token %}

                {% if form.errors %}
                    <p class="alert alert-danger">{{ form.errors }}</p>
                {% endif %}
                <div class="media">
                    {% if profile.picture %}
                        <img src=" {{ profile.picture.url }} " alt="Profile picture" class="rounded-circle" height="50">
                    {% else %}
                        <img src="{% static 'img/default-profile.png' %}" alt="Profile picture" class="rounded-circle" height="50">
                    {% endif %}
                    
                    <div class="media-body">
                        <h5 class="ml-4">@{{ user.username }} | {{ user.get_full_name }}</h5>
                        <p class="ml-4"><input type="file" name="picture" required="true"></p>
                    </div>
                </div>
                
                <hr><br>

                <div class="form-group">
                    <label for="website">Website</label>
                    <input 
                        type="url" 
                        class="form-control" 
                        name="website" 
                        placeholder="Website"
                        value="{{ profile.website }}"
                    >
                </div>

                <div class="form-group">
                    <label for="biography">Biography</label>
                    <textarea name="biography" id="" cols="30" rows="10" class="form-control">{{ profile.biography }}</textarea>
                </div>

                <div class="form-group">
                    <label for="phone_number">Phone number</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        name="phone_number" 
                        placeholder="Phone number" 
                        value="{{ profile.phone_number }}">
                </div>

                <button type="submit" class="btn btn-primary btn-block mt-5">Update info</button>

            </form>
        </div>
    </div>

{% endblock container %}
````
- Se cambia `update_profile` por `users:update`.

### templates/nav.html
````html
{% load static %}
<nav class="navbar navbar-expand-lg fixed-top" id="main-navbar">
    <div class="container">

        <a class="navbar-brand pr-5" style="border-right: 1px solid #efefef;" href="{% url 'posts:feed' %}">
            <img src="{% static "img/instagram.png" %}" height="45" class="d-inline-block align-top"/>
        </a>

        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item">
```
                    <a href="{% url 'users:detail' request.user.username %}">
```
                        {% if request.user.profile.picture %}
                            <img src="{{ request.user.profile.picture.url }}" height="35" class="d-inline-block align-top rounded-circle"/>
                        {% else %}
                            <img src="{% static 'img/default-profile.png' %}" height="35" class="d-inline-block align-top rounded-circle"/>
                        {% endif %}
                        
                    </a>
                </li>

                <li class="nav-item nav-icon">
```
                    <a href="{% url 'posts:create' %}">
```
                        <i class="fas fa-plus"></i>
                    </a>
                </li>

                <li class="nav-item nav-icon">
                    <a href="{% url "users:logout" %}">
                        <i class="fas fa-sign-out-alt"></i>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
````
- Se incluye el enlace a `users:details`, y el valor `request.user.username`

### templates/posts/feed.html
````html
{% extends "base.html" %}

{% block head_content %}
<title>Instagram feed</title>
{% endblock head_content %}

{% block container %}
    <div class="container">
        <div class="row">
      
            {% for post in posts %}
                <div class="col-sm-12 col-md-8 offset-md-2 mt-5 p-0 post-container">
                    <div class="media pt-3 pl-3 pb-1">
```
                        <a href="{% url 'users:detail' post.user.username %}">
```
                            <img 
                            src="{{ post.profile.picture.url }}" 
                            alt="{{ post.user.get_full_name }}"
                            class="mr-3 rounded-circle" 
                            height="35"
                            >
```
                        </a>
```
                        
                        <div class="media-body">
                            <p style="margin-top: 5px;">{{ post.user.get_full_name }}</p>
                        </div>
                    </div>

                    <img src="{{ post.photo.url }}" alt="{{ post.title }}" style="width: 100%;">

                    <p class="mt-1 ml-2">
                        <a href="" style="color: #000; font-size: 20px;">
                            <i class="far fa-heart"></i>
                        </a> 30 likes
                    </p>
                    <p class="ml-2 mt-0 mb-2">
                        <b>{{ post.title }}</b> - <small>{{ post.created }}</small>
                    </p>
                </div>
            {% endfor %}
        </div>
    </div>
{% endblock container %}
````
- Se incluye el enlace a `users:details`, y el valor `post.user.username`

### posts/views.py
````py
'''Posts views.'''
# Django
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
```
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
```

# Forms
from posts.forms import PostForm

# Models
from posts.models import Post

# Create your views here.
```
class PostFeedView(LoginRequiredMixin, ListView):
    """Return all publish posts."""
    template_name = 'posts/feed.html'
    model = Post
    ordering = ('-created',)
    paginate_by = 2
    context_object_name = 'posts'
```

@login_required
def create_post(request):
    """Create new post view."""
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
```
            return redirect('posts:feed')
```
    else:
        form = PostForm()
    
    return render(
        request=request,
        template_name='posts/new.html',
        context={
            'form': form,
            'user': request.user,
            'profile': request.user.profile,
        }
    )
````
- Se cambia todos los enlaces `xxx` por `users:xxx`.
- Incluimos `LoginRequiredMixin` y `ListView`.
- Eliminamos `list_posts` y lo remplazamos por la clase `PostFeedView` que hereda de `LoginRequiredMixin` y `ListView`.

### posts/urls.py
````py
"""Posts URLs."""

# Django
from django.urls import path

# Views
from posts import views

urlpatterns = [
    path(
        route="",
```
        view=views.PostFeedView.as_view(),
```
        name="feed"
    ),
    path(
        route="posts/new/",
        view=views.create_post,
        name="create"
    ),

    
]
````
- Se cambia `list_post` por `PostFeedView`

### templates/users/login.html
````html
{% extends 'users/base.html' %}

{% block head_content %}
    <title>Instagram sign in</title>
{% endblock head_content %}

{% block container %}

    {% if error %}
        <p class="alert alert-danger">{{ error }}</p>
    {% endif %}
```
    <form method="POST" action='{% url "users:login" %}'>
```
        {% csrf_token %}
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Username" name="username">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" name="password">
        </div>
        <button class="btn btn-primary btn-block mt-5" type="submit">Sing in!</button>
    </form>
```
    <p class="mt-4">Don't have an account yet? <a href="{% url 'users:signup' %}">Sign up here.</a></p>
```
{% endblock container %}
````
### templates/users/signup.html

````html
{% extends 'users/base.html' %}

{% block head_content %}
    <title>Instagram sign up</title>
{% endblock head_content %}

{% block container %}
```
    <form method="POST" action='{% url "users:signup" %}'>
```
        {% csrf_token %}

        {{ form.as_p }}

        <button class="btn btn-primary btn-block mt-5" type="submit">Register!</button>
    </form>

{% endblock container %}
````
### Lecturas recomendadas
- [Django Documentation - Built-in class-based views API](https://docs.djangoproject.com/en/2.0/ref/class-based-views/)
- [Classy Class-Based Views](http://ccbv.co.uk/)



## CreateView, FormView y UpdateView
Se incorpora la paginación de posts utilizando page_obj, bootstrap y variables del contexto. Adicionalmente se utiliza DetailView para la creación de una vista de detalle de un post. Se remplaza render y redirect por CreateView para los posts. Se implementa FormView en sustitución del formulario de registro Signup que teníamos hasta ahora con el fin de optimizar el código y finalmente se optimiza la vista de actualización del perfil con UpdateView.

- Se reemplaza `xxx` por `users:xxx` en las urls

### templates/pagination.html
````
.
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── pagination.html
│   ├── posts
│   │   ├── feed.html
│   │   └── new.html
│   └── users
│       ├── base.html
│       ├── detail.html
│       ├── login.html
│       ├── signup.html
│       └── update_profile.html
:    :    :
:    :    :
````
````html
<nav>
    <ul class="pagination justify-content-end">
        {% if page_obj.has_previous %}
        <li class="page-item">
            <a href="?page={{ page_obj.previous_page_number }}" class="page-link">
                Previous
            </a>
        </li>
        {% endif %}
        <li class="page-item">
            <a href="#" class="page-link">
                {{ page_obj.number }} of {{ page_obj.paginator.num_pages }}
            </a>
        </li>
        {% if page_obj.has_next %}
        <li class="page-item">
            <a href="?page={{ page_obj.next_page_number }}" class="page-link">
                Next
            </a>
        </li>
        {% endif %}
    </ul>
</nav>
````
- Se crea un template `pagination.html` que contiene una barra de navegación con los botones de paginación, `previous_page_number`, `number`, `num_pages`, `next_page_number`.

### posts/urls.py
````py
"""Posts URLs."""

# Django
from django.urls import path

# Views
from posts import views

urlpatterns = [
    path(
        route="",
        view=views.PostFeedView.as_view(),
        name="feed"
    ),
    path(
        route="posts/new/",
```
        view=views.CreatePostView.as_view(),
```
        name="create"
    ),
```
    path(
        route="posts/<int:pk>/",
        view=views.PostDetailView.as_view(),
        name="detail"
    ),
```
]
````
- Se crea una ruta para el detalle de los post, que utiliza la vista `PostDetailView`.
- Se cambia `views.create_post` por `views.CreatePostView`.
### post/views.py
````py
'''Posts views.'''
# Django
```
from django.urls import reverse_lazy
```
from django.contrib.auth.mixins import LoginRequiredMixin
```
from django.urls.base import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView
```

# Forms
from posts.forms import PostForm

# Models
from posts.models import Post

# Create your views here.
class PostFeedView(LoginRequiredMixin, ListView):
    """Return all publish posts."""
    template_name = 'posts/feed.html'
    model = Post
    ordering = ('-created',)
```
    paginate_by = 30
```
    context_object_name = 'posts'

```
class PostDetailView(LoginRequiredMixin, DetailView):
    """Return post detail."""

    template_name = 'posts/detail.html'
    queryset = Post.objects.all()
    context_object_name = 'post'

class CreatePostView(LoginRequiredMixin, CreateView):
    """Create a new post."""
    template_name = 'posts/new.html'
    form_class = PostForm
    success_url = reverse_lazy('posts:feed')

    def get_context_data(self, **kwargs):
        """Add user and profile to context."""
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        context['profile'] = self.request.user.profile
        return context
```
````
- Se crea una clase `PostDetailView` que hereda a `LoginRequiredMixin` y `DetailView`.
- Se abia la paginación de `paginate_by = 2` a `paginate_by = 30`.
- Se reemplaza `create_post` por `CreatePostView` que hereda de una clase `CreateView`.

### templates/posts/feed.html
````html
{% extends "base.html" %}

{% block head_content %}
<title>Instagram feed</title>
{% endblock head_content %}

{% block container %}
    <div class="container">
        <div class="row">
      
            {% for post in posts %}
```
                {% include "posts/post_card.html" %}
```
            {% endfor %}
        </div>
    </div>
```
    {% include "pagination.html" %}
```
{% endblock container %}

````
- Se inlcluye `pagination.html` en la parte final.
- Se incluye `post_card.html` para que contenga la información de cada post, y se elimina el código anterior dedicado a la presentación de los posts.

### templates/posts/detail.html
````
.
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── pagination.html
│   ├── posts
│   │   ├── detail.html
│   │   ├── feed.html
│   │   └── new.html
│   └── users
│       ├── base.html
│       ├── detail.html
│       ├── login.html
│       ├── signup.html
│       └── update_profile.html
:    :    :
:    :    :
````
````html
{% block head_content %}
    <title>Instagram</title>
{% endblock head_content %}
<!DOCTYPE html>
{% block container %}

    <div class="container">
        <div class="row">
            {% include "posts/post_card.html" %}
        </div>
    </div>
{% endblock container %}
````
- Se crea un template `detail.html` para presentar el detalle de cada uno de los post.
- Incluye un `post_card.html`

### tempates/posts/post_card.html
````
.
├── templates
│   ├── base.html
│   ├── nav.html
│   ├── pagination.html
│   ├── posts
│   │   ├── detail.html
│   │   ├── feed.html
│   │   ├── new.html
│   │   └── post_card.html
│   └── users
│       ├── base.html
│       ├── detail.html
│       ├── login.html
│       ├── signup.html
│       └── update_profile.html
:    :    :
:    :    :
````
````html
<div class="col-sm-12 col-md-8 offset-md-2 mt-5 p-0 post-container">
    <div class="media pt-3 pl-3 pb-1">
        <a href="{% url 'users:detail' post.user.username %}">
            <img src="{{ post.profile.picture.url }}" alt="{{ post.user.get_full_name }}" class="mr-3 rounded-circle" height="35">
        </a>
        <div class="media-body">
            <p style="margin-top: 5px;">
                {{ post.user.get_full_name }}
            </p>
        </div>
    </div>

    <img src="{{ post.photo.url }}" alt="{{ post.title }}" style="width: 100%;">

    <p class="mt-1 ml-2">
        <a href="" style="color: #000; font-size: 20px;">
            <i class="far fa-heart"></i>
        </a> 30 likes
    </p>
    <p class="ml-2 mt-0 mb-2">
        <b>{{ post.title }}</b> - <small>{{ post.created }}</small>
    </p>
</div>
````
- Se crear un template `post_card.html` que contiene los post, y se pude utilizar en diferentes templates, como `detail.html` y `feed.html`.

### users/views.py
````py
"""Users views."""

# Django
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render
```
from django.urls import reverse, reverse_lazy
from django.views.generic import DetailView, FormView, UpdateView
```

# Models
from django.contrib.auth.models import User
from posts.models import Post
```
from users.models import Profile
```

# Forms
```
from users.forms import SignupForm
```
queryset = User.objects.all()

# Create your views here.

class UserDetailView(LoginRequiredMixin, DetailView):
    """User detail view."""

    template_name = 'users/detail.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'
    queryset = User.objects.all()
    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        """Add user's posts to context"""
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        context['posts'] = Post.objects.filter(user=user).order_by('-created')
        return context

```
class UpdateProfileView(LoginRequiredMixin, UpdateView):
    """Update profile view."""

    template_name = 'users/update_profile.html'
    model = Profile
    fields = ['website', 'biography', 'phone_number', 'picture']

    def get_object(self):
        """Return user's profile."""
        return self.request.user.profile

    def get_success_url(self):
        """Return to user's profile."""
        username = self.object.user.username
        return reverse('users:detail', kwargs={'username': username})
```

def login_view(request):
    """Login view."""
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('posts:feed')
        else:
            return render(request, 'users/login.html', {'error': 'Invalid username or password'})

    return render(request, 'users/login.html')

```
class SignupView(FormView):
    """Users sign up view."""

    template_name = 'users/signup.html'
    form_class = SignupForm
    success_url = reverse_lazy('users:login')

    def form_valid(self, form):
        """Save form data."""
        form.save()
        return super().form_valid(form)
```

@login_required
def logout_view(request):
    """Logout a user."""
    logout(request)
    return redirect('users:login')
````
- Se reemplaza `signup_view` por `SignupView` que hereda de `FormView`.
- Se reemplaza `update_profile` por `UpdateProfileView` que hereda de `LoginRequiredMixin` y `UpdateView`.
### users/urls.py
````py
"""Users URLs."""

# Django
from django.urls import path
```
from users.views import SignupView
```

# Views
from users import views

urlpatterns = [
    

    # Management
    path(
        route='login/',
        view=views.login_view,
        name='login'
    ),
    path(
        route='logout/',
        view=views.logout_view,
        name='logout'
    ),
    path(
        route='signup/',
```
        view=views.SignupView.as_view(),
```
        name='signup'
    ),
    path(
        route='me/profile/',
```
        view=views.UpdateProfileView.as_view(),
```
        name='update'
    ),

```    
    # Posts
    path(
        route='<str:username>/',
        view=views.UserDetailView.as_view(),
        name='detail'
    )
```
    
]
````
- Se cambia `views.signup_view` por `views.SigupView`.
- Se cambia el orden de las urls, y se baja `UserDetailView`, para que no afecte las otras, ya que al ser más genérica, oculta a las otras, por ejemplo a `SignupView`
- Se cambia `views.update_profile` por `views.UpdateProfileView`.

### webapp/middleware.py
````py
"""Instagram middleware catalog."""

# Django
from django.shortcuts import redirect
from django.urls import reverse

class ProfileCompletionMiddleware:
    """Profile completion middleware
    
    Ensure every user that is interactin with the platform
    have their profile picture and biography.
    """

    def __init__(self, get_response):
        """Middleware initialization"""
        self.get_response = get_response

    def __call__(self, request):
        """Code to be executed for each request before the view is called."""
        if not request.user.is_anonymous:
            if not request.user.is_staff:
                profile = request.user.profile
                if not profile.picture or not profile.biography:
```
                    if request.path not in [reverse('users:update'), reverse('users:logout')]:
                        return redirect('users:update')
```
        
        response = self.get_response(request)
        return response
````
- Se ajustan las urls para que cambien de `xxx` a `users:xxx`.

### users/forms.py
````py
"""User forms."""

# Django
from django import forms

# Models
from django.contrib.auth.models import User
from users.models import Profile

class SignupForm(forms.Form):
    """Sign up form."""
    username = forms.CharField(min_length=4, max_length=50)
    password = forms.CharField(
        max_length=70,
        widget=forms.PasswordInput()
        )

    password_confirmation = forms.CharField(
        max_length=70,
        widget=forms.PasswordInput()
        )

    first_name = forms.CharField(min_length=2, max_length=50)
    last_name = forms.CharField(min_length=2, max_length=50)

    email = forms.CharField(
        min_length=6, 
        max_length=70,
        widget=forms.EmailInput()
        )

    def clean_username(self):
        """Username must be unique."""
        username = self.cleaned_data['username']
        username_taken = User.objects.filter(username=username).exists()
        if username_taken:
            raise forms.ValidationError('Username is already in use.')
        return username

    def clean(self):
        """Verify password confirmation match."""
        data = super().clean()

        password = data['password']
        password_confirmation = data['password_confirmation']

        if password != password_confirmation:
            raise forms.ValidationError('Password do not match.')

        return data

    def save(self):
        """Create user and profile."""
        data = self.cleaned_data
        data.pop('password_confirmation')

        user = User.objects.create_user(**data)
        profile = Profile(user=user)
        profile.save()

```

```
````
- Se elimina `ProfileForm` ya que era utilziado por `views.signup_view`, el cual se eliminó, y ya no será utilizado.

### Lecturas recomendadas
- [CreateView](http://ccbv.co.uk/projects/Django/2.0/django.views.generic.edit/CreateView/)
- [FormView](http://ccbv.co.uk/projects/Django/2.0/django.views.generic.edit/FormView/)
- [UpdateView](http://ccbv.co.uk/projects/Django/2.0/django.views.generic.edit/UpdateView/)




## Generic auth views
Django cuenta con varias vistas genéricas basadas en clases para resolver muchas de las funcionalidades relacionadas con la autenticación, como es el caso de:

- login
- logout
- password_change
- password_change_done
- password_reset
- password_reset_done
- password_reset_confirm
- password_reset_complete

Estas vistas genéricas nos permiten ahorrarnos varias líneas de código, además de que incluyen características adicionales de mucha utilidad.

### users/views.py
````py
"""Users views."""

# Django
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
```
from django.contrib.auth import views as auth_views
```
from django.urls import reverse, reverse_lazy
from django.views.generic import DetailView, FormView, UpdateView

# Models
from django.contrib.auth.models import User
from posts.models import Post
from users.models import Profile

# Forms
from users.forms import SignupForm
queryset = User.objects.all()

# Create your views here.

class UserDetailView(LoginRequiredMixin, DetailView):
    """User detail view."""

    template_name = 'users/detail.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'
    queryset = User.objects.all()
    context_object_name = 'user'

    def get_context_data(self, **kwargs):
        """Add user's posts to context"""
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        context['posts'] = Post.objects.filter(user=user).order_by('-created')
        return context


class UpdateProfileView(LoginRequiredMixin, UpdateView):
    """Update profile view."""

    template_name = 'users/update_profile.html'
    model = Profile
    fields = ['website', 'biography', 'phone_number', 'picture']

    def get_object(self):
        """Return user's profile."""
        return self.request.user.profile

    def get_success_url(self):
        """Return to user's profile."""
        username = self.object.user.username
        return reverse('users:detail', kwargs={'username': username})

```
class LoginView(auth_views.LoginView):
    """Login view."""

    template_name = 'users/login.html'
```

class SignupView(FormView):
    """Users sign up view."""

    template_name = 'users/signup.html'
    form_class = SignupForm
    success_url = reverse_lazy('users:login')

    def form_valid(self, form):
        """Save form data."""
        form.save()
        return super().form_valid(form)

```
class LogoutView(LoginRequiredMixin, auth_views.LogoutView):
    """Logout view."""

    template_name = 'users/logged_out.html'
```
````
- Se cambia la función `login_view` por la clase `LoginView` la cual hereda de `LoginView`.
- Se cambia la función `logout_view` por la calse `LogoutView` la cual hereda de `LoginRequiredMixin` y `LogoutView`.

### templates/users/login.html
````html
{% extends 'users/base.html' %}

{% block head_content %}
    <title>Instagram sign in</title>
{% endblock head_content %}

{% block container %}

```
    {% if form.errors %}
        <p class="alert alert-danger">
            Your username and password didn't match. Please try again.
        </p>
    {% endif %}
```

    <form method="POST" action='{% url "users:login" %}'>
        {% csrf_token %}
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Username" name="username">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" name="password">
        </div>
        <button class="btn btn-primary btn-block mt-5" type="submit">Sing in!</button>
    </form>

    <p class="mt-4">Don't have an account yet? <a href="{% url 'users:signup' %}">Sign up here.</a></p>

{% endblock container %}

````
- Dentro del `if` se cambia `error` por `form.errors`, y el único que error que puede haber es que los datos no estén correctos, se ajusta el párrafo de los errores para que indique cuando se genera el error.

### users/urls.py
````py
"""Users URLs."""

# Django
from django.urls import path
from users.views import SignupView

# Views
from users import views

urlpatterns = [
    

    # Management
    path(
        route='login/',
```
        view=views.LoginView.as_view(),
```
        name='login'
    ),
    path(
        route='logout/',
```
        view=views.LogoutView.as_view(),
```
        name='logout'
    ),
    path(
        route='signup/',
        view=views.SignupView.as_view(),
        name='signup'
    ),
    path(
        route='me/profile/',
        view=views.UpdateProfileView.as_view(),
        name='update'
    ),
    
    # Posts
    path(
        route='<str:username>/',
        view=views.UserDetailView.as_view(),
        name='detail'
    )

    
]
````
- Se cambia `views.login_view` por `LoginView`.
- Se cambia `views.logout_view` por `LogoutView`.

### webapp/settings.py
````py
:    :    :
:    :    :
LOGIN_URL = '/users/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = LOGIN_URL
````
- Se indica que el `LOGIN_REDIRECT_URL` es `/`, y que `LOGOUT_REDIRECT_URL` es igual a `LOGIN_URL`.

### Lecturas recomendadas
- [Django Documentation - Using the Django authentication system - Authentication Views](https://docs.djangoproject.com/en/2.0/topics/auth/default/#module-django.contrib.auth.views)



## Glosario
- __ORM__: Object-relational mapping. Es el encargado de permitir
el acceso y control de una base de datos relacional a través de
una abstracción a clases y objetos.
- __Templates__: Archivos HTML que permiten la inclusión y ejecución
de lógica especial para la presentación de datos.
- __Modelo__: Parte de un proyecto de Django que se encarga de estructurar
las tablas y propiedades de la base de datos a través de clases de Python.
- __Vista__: Parte de un proyecto de Django que se encarga de la
lógica de negocio y es la conexión entre el template y el modelo.
- __App__: Conjunto de código que se encarga de resolver una parte
muy específica del proyecto, contiene sus modelos, vistas, urls, etc.
- __Patrón de diseño__: Solución común a un problema particular.